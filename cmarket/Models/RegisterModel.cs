﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace cmarket.Models
{
    public class RegisterModel
    {


        public int id_owner { get; set; }
        public string o_name { get; set; }
        public string o_surname { get; set; }
        public string o_idcard { get; set; }
        public string o_age { get; set; }
        public string o_career { get; set; }
        public string o_address { get; set; }
        public string o_road { get; set; }

        public string o_phone { get; set; }
        public string o_email { get; set; }
        public string o_lineid { get; set; }
        public string o_date { get; set; }
        public string o_status { get; set; }
        public string o_pass { get; set; }

        public int PROVINCE_ID { get; set; }
        public string PROVINCE_NAME { get; set; }


        public int AMPHUR_ID { get; set; }
        public string AMPHUR_NAME { get; set; }

        public int DISTRICT_ID { get; set; }
        public string DISTRICT_NAME { get; set; }

        public int type_land_id { get; set; }
        public string name_type { get; set; }

        public int img_id { get; set; }
        public string img_name { get; set; }


        public int o_land_id { get; set; }
        public string land_rai { get; set; }
        public string land_nga { get; set; }
        public string land_va { get; set; }
        public string m_rent { get; set; }
        public string period_per { get; set; }
        public string period { get; set; }
        public string codition { get; set; }
        public string dsave { get; set; }
        public string place_land { get; set; }
        public string latitude_land { get; set; }
        public string longitude_land { get; set; }
        public string status_land { get; set; }
        public string ccl { get; set; }

        public string type_owner_land { get; set; }

        public int type_organi_id { get; set; }
        public string name_organi { get; set; }


        public int reg_organi_id { get; set; }
        public string reg_name { get; set; }
        public string reg_numberorgani { get; set; }
        public string reg_regis_name { get; set; }
        public string reg_posi_name { get; set; }
        public string reg_regis_tel { get; set; }
        public string reg_email { get; set; }
        public string reg_chief { get; set; }
        public string reg_ctel { get; set; }
        public string reg_vchief { get; set; }
        public string reg_vctel { get; set; }
        public string reg_num { get; set; }
        public string reg_addr { get; set; }
        public string detail_opara { get; set; }
        public string activity_curent { get; set; }
        public string reg_results { get; set; }
        public string reg_vision { get; set; }
        public string reg_plant { get; set; }
        public string reg_rai { get; set; }
        public string reg_nga { get; set; }
        public string reg_wa { get; set; }
        public string reg_member { get; set; }
        public string reg_income { get; set; }
        public string reg_capital { get; set; }
        public string reg_accept { get; set; }
        public string reg_status { get; set; }
        public string reg_password { get; set; }



        public int polygon_land_id { get; set; }
        public string polygon_lata { get; set; }
        public string polygon_ing { get; set; }


        public int owner_user_land_id { get; set; }


        public int id_land_use { get; set; }
        public string land_use_name { get; set; }

        public int id_data_water { get; set; }
        public string water_name { get; set; }

        public int id_land_group { get; set; }
        public string land_group_name { get; set; }

        public int id_condi_use_land { get; set; }
        public string condi_name { get; set; }

        public int seq_num { get; set; }

        public string detail_user_land { get; set; }

        
       public int id_land_water { get; set; }

        public string land_group_detail { get; set; }
        public int id_r_land_group { get; set; }

        public int id_r_condi_land { get; set; }
        public string condi_detail { get; set; }

        public int condi_seq { get; set; }

        public int land_use_seq { get; set; }

        public int o_wland { get; set; }

        public int type_status_regis { get; set; }

        public int f_wland { get; set; }

        public string far_code { get; set; }
        public int id_land_famer { get; set; }
        
    }

    public class RegisterFarnerModel
    {
        public int id_career { get; set; }
        public string career_name { get; set; }
        public string id_type_career { get; set; }


        public int id_type_farmer { get; set; }
        public string type_fatmer_name { get; set; }

        public int r_career_id { get; set; }
        public string far_id { get; set; }

        public int id_land_famer { get; set; }

        public int PROVINCE_ID { get; set; }
        public int AMPHUR_ID { get; set; }
        public int DISTRICT_ID { get; set; }

        public string land_rai { get; set; }
        public string land_nga { get; set; }
        public string land_va { get; set; }
        public int type_land_id { get; set; }

        public string PROVINCE_NAME { get; set; }
        public string AMPHUR_NAME { get; set; }
        public string DISTRICT_NAME { get; set; }


        public int farmer_user_land_id { get; set; }
        public int id_land_use { get; set; }
        public string fdetail_user_land { get; set; }

        public string land_use_name { get; set; }
        public int f_wland { get; set; }
        public string reg_member { get; set; }
        public string reg_income { get; set; }
        public string reg_capital { get; set; }

        
        

    }

}