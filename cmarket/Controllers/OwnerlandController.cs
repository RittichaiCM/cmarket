﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cmarket.Models;
using MySql.Data.MySqlClient;

namespace cmarket.Controllers
{
    public class OwnerlandController : Controller
    {
        private cmarket_dbEntities db = new cmarket_dbEntities();

        // GET: tb_ownerland
        public ActionResult Index()
        {
            return View(db.tb_ownerland.ToList());
        }

        // GET: tb_ownerland/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_ownerland tb_ownerland = db.tb_ownerland.Find(id);
            if (tb_ownerland == null)
            {
                return HttpNotFound();
            }
            return View(tb_ownerland);
        }

        // GET: tb_ownerland/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tb_ownerland/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_owner,o_name,o_surname,o_idcard,o_age,o_career,o_address,o_road,PROVINCE_ID,AMPHUR_ID,DISTRICT_ID,o_phone,o_email,o_lineid,o_date,o_status")] tb_ownerland tb_ownerland)
        {
            if (ModelState.IsValid)
            {
                db.tb_ownerland.Add(tb_ownerland);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tb_ownerland);
        }

        // GET: tb_ownerland/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_ownerland tb_ownerland = db.tb_ownerland.Find(id);
            if (tb_ownerland == null)
            {
                return HttpNotFound();
            }
            return View(tb_ownerland);
        }

        // POST: tb_ownerland/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_owner,o_name,o_surname,o_idcard,o_age,o_career,o_address,o_road,PROVINCE_ID,AMPHUR_ID,DISTRICT_ID,o_phone,o_email,o_lineid,o_date,o_status")] tb_ownerland tb_ownerland)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tb_ownerland).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tb_ownerland);
        }

        // GET: tb_ownerland/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tb_ownerland tb_ownerland = db.tb_ownerland.Find(id);
            if (tb_ownerland == null)
            {
                return HttpNotFound();
            }
            return View(tb_ownerland);
        }

        // POST: tb_ownerland/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tb_ownerland tb_ownerland = db.tb_ownerland.Find(id);
            db.tb_ownerland.Remove(tb_ownerland);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
