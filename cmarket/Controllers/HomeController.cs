﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cmarket.Models;
using MySql.Data.MySqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using PagedList;

namespace cmarket.Controllers
{
    public class HomeController : Controller
    {
        string constr = ConfigurationManager.ConnectionStrings["connectmysql"].ConnectionString;
        
        public ActionResult Index(int? page)
        {

            ViewBag.Message = "temindex";

            /*  List<RegisterModel> ownerland = new List<RegisterModel>();
            //  string constr = null;
              //string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
              string constr = ConfigurationSettings.AppSettings["cmarket_dbEntities"];

              using (MySqlConnection con = new MySqlConnection(constr))
              {
                  string query = "SELECT id_owner, o_name, o_surname FROM tb_ownerland";
                  using (MySqlCommand cmd = new MySqlCommand(query))
                  {
                      cmd.Connection = con;
                      con.Open();
                      using (MySqlDataReader sdr = cmd.ExecuteReader())
                      {
                          while (sdr.Read())
                          {
                              ownerland.Add(new RegisterModel
                              {
                                  id_owner = Convert.ToInt32(sdr["id_owner"]),
                                  o_name = sdr["o_name"].ToString(),
                                  o_surname = sdr["o_surname"].ToString()
                              });
                          }
                      }
                      con.Close();
                  }
              }*/


            // MySqlConnection connection = new MySqlConnection("Database=cmarket_db;Data Source=localhost;User Id=root;Password=''");
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

           /* command.CommandText = "select " +
               "t.name_type,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land from owner_land as ow " +
               "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
               "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
               "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
               "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
               "where ow.status_land='2'";*/

            command.CommandText = "select " +
                "count(ow.o_land_id) as ccl,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.period_per," +
                "ow.period,ow.type_land_id,ow.dsave,ow.land_rai,ow.land_nga,ow.land_va," +
                "ow.codition,ow.m_rent,ow.place_land,ow.status_land," +
                "ow.latitude_land,ow.longitude_land,t.name_type,dd.img_name,ow.o_wland from owner_land as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +
                "left join type_land as t on(t.type_land_id=ow.type_land_id)" +


                "where ow.status_land='2'" +
                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave order by ow.o_land_id desc limit 8";

            MySqlDataReader reader = command.ExecuteReader();



            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    o_land_id = Convert.ToInt32(reader["o_land_id"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    dsave = reader["dsave"].ToString(),
                    m_rent = reader["m_rent"].ToString(),
                    place_land = reader["place_land"].ToString(),
                    type_land_id = Convert.ToInt32(reader["type_land_id"]),
                    land_rai = reader["land_rai"].ToString(),
                    land_nga = reader["land_nga"].ToString(),
                    land_va = reader["land_va"].ToString(),
                    period_per = reader["period_per"].ToString(),
                    period = reader["period"].ToString(),
                    codition = reader["codition"].ToString(),
                    latitude_land = reader["latitude_land"].ToString(),
                    longitude_land = reader["longitude_land"].ToString(),
                    name_type = reader["name_type"].ToString(),
                    img_name = reader["img_name"].ToString(),
                    o_wland = Convert.ToInt32(reader["o_wland"]),

                });
            }


            reader.Close();


            command.CommandText = "select count(*) as countperson from tb_regfarmer as ow where ow.type_status_regis='1'";

            MySqlDataReader reader2 = command.ExecuteReader();

            if (reader2.Read())
            {
                ViewData["countperson"] = Convert.ToInt32(reader2["countperson"]);
                
            }
            reader2.Close();

            command.CommandText = "select count(*) as countog from regis_organi as ow where ow.type_status_regis_og='1'";

            MySqlDataReader reader22 = command.ExecuteReader();

            if (reader22.Read())
            {
                ViewData["countog"] = Convert.ToInt32(reader22["countog"]);

            }
            reader22.Close();


            command.CommandText = "select count(*) as countwhatperson from tb_regfarmer as ow where ow.type_status_regis='2'";

            MySqlDataReader reader3 = command.ExecuteReader();

            if (reader3.Read())
            {
                ViewData["countwhatperson"] = Convert.ToInt32(reader3["countwhatperson"]);

            }
            reader3.Close();

            command.CommandText = "select count(*) as countwahtog from regis_organi as ow where ow.type_status_regis_og='2'";

            MySqlDataReader reader33 = command.ExecuteReader();

            if (reader33.Read())
            {
                ViewData["countwahtog"] = Convert.ToInt32(reader33["countwahtog"]);

            }
            reader33.Close();


            command.CommandText = "select COUNT(DISTRICT_ID) as landwant from land_famer";

            MySqlDataReader reader4 = command.ExecuteReader();

            if (reader4.Read())
            {
                ViewData["landwant"] = Convert.ToInt32(reader4["landwant"]);

            }
            reader4.Close();


            /* var news = context.Include(m => m.Attachments).OrderByDescending(m => m.Pined).ThenBy(m => m.CreatedAt);
             int pageSize = 8;
             int pageNumber = (page ?? 1);*/

            ViewBag.land = land;
            return View();
        }


        public ActionResult RegisterLand()
        {


            
           ViewBag.Message = "regisowner";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    
                });
            }


            reader.Close();




            TempData["capchar"] = capchar(7);

            return View("Index",ownerland);
        }

        public ActionResult SubmitRegister()
        {

            /* ViewData["firstname"] = o_name;
             ViewData["lastname"] = o_surname;
             ViewData["email"] = o_idcard;*/

            string o_name = Request.Form["o_name"];
            string o_surname = Request.Form["o_surname"];
            string o_idcard = Request.Form["o_idcard"];
            string o_age = Request.Form["o_age"];
            string o_career = Request.Form["o_career"];
            string o_address = Request.Form["o_address"];
            string o_road = Request.Form["o_road"];

            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string amp_id = Request.Form["amp_id"];
            string distr_id = Request.Form["distr_id"];

            string o_phone = Request.Form["o_phone"];
            string o_email = Request.Form["o_email"];
            string o_lineid = Request.Form["o_lineid"];
            string o_pass = CreatePassword(5);

            var pass = GenerateSHA512String(o_pass);


           // string Role = "Role,Rolename,Rolepwd";
            string[] DateArray = o_age.Split('-');
            string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];

            /* ViewData["insert_owner"] = "owner";
             ViewData["lastname"] = o_surname;
             ViewData["email"] = o_idcard;

            ViewData["PROVINCE_ID"] = PROVINCE_ID;
            ViewData["amp_id"] = amp_id;
            ViewData["distr_id"] = distr_id;*/

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "insert into tb_ownerland " +
                "(o_name," +
                "o_surname," +
                "o_idcard," +
                "o_age," +
                "o_career," +
                "o_address," +
                "o_road," +
                "PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "o_phone," +
                "o_email," +
                "o_lineid," +
                "o_pass" +

                ")" +
                "VALUES " +
                "('"+o_name+"'," +
                "'"+o_surname+"'," +
                "'"+o_idcard+"'," +
                "'"+ datebirth + "'," +
                "'"+o_career+"'," +
                "'"+o_address+"'," +
                "'"+o_road+"'," +
                "'"+PROVINCE_ID+"'," +
                "'"+amp_id+"'," +
                "'"+distr_id+"'," +
                "'"+o_phone+"'," +
                "'"+o_email+"'," +
                "'"+o_lineid+"'," +
                "'"+ pass + "'" +
                ")";
            int i = command.ExecuteNonQuery();
            var id_owner = command.LastInsertedId;
            
            var changeid = CreatePassword(3)+ id_owner+ CreatePassword(50);
            connection.Close();

            
            //return View("About");

             if (i >= 1)
            {
               
                string Url = "wwww.google.com";


                TempData["data_owner"] = o_name + "," + o_surname + "," + o_pass + "," + o_email;
                TempData["Url"] = Url;

                //ViewBag.Message = "owner";
                //ViewBag.check = "true";

                TempData["owner"] = "owner";
                TempData["check"] = "true";
                TempData["id_owner"] = changeid;



                return RedirectToAction("confrim_regis", "Home");

                //return Redirect("http://intranet.labai.or.th:8080/apimail/getmail.php?data=" + data);
                //return View("Index", data);
                //Response.Redirect("http://intranet.labai.or.th:8080/apimail/getmail.php?data="+data);
                //return Redirect("http://www.google.com");
            }
            else
            {

               // ViewBag.Message = "owner";
                //ViewBag.check = "false";
                TempData["owner"] = "owner";
                TempData["check"] = "false";

                return RedirectToAction("confrim_regis", "Home");
            }
            
        }

        public ActionResult conf(string id)
        {

            
                        
            string far_id = id.Substring(3,id.Length -53);
            ViewBag.textid = far_id;

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update tb_regfarmer set " +
                     "regis_confrim ='2'" +
                     " where far_id = '" + far_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1) {
                ViewBag.Message = "login";
                return View("index");
            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }


            
        }


        public ActionResult changepass(string id)
        {

            ViewBag.Message = "fromchangepass";

            string id_owner = id.Substring(3, id.Length - 53);
            //string id_owner = id.Substring(0,53);
            ViewBag.textid = id_owner;

            return View("index");



        }


        public ActionResult changepassfarmer(string id)
        {

            ViewBag.Message = "fromchangepassfarmer";

            string far_id = id.Substring(3, id.Length - 53);
            //string id_owner = id.Substring(0,53);
            ViewBag.textid = far_id;

            return View("index");



        }


        public ActionResult submitchpass()
        {
            string o_pass = Request.Form["o_pass"];
            //string id_owner = Request.Form["id_owner"];
            var pass = GenerateSHA512String(o_pass);
            string id_enc = Request.Form["id_enc"];
            


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            if (@Session["id_owner"] != null)
            {
                command.CommandText = "update tb_ownerland set " +
                     "o_pass ='" + pass + "'" +
                     " where id_owner = '" + @Session["id_owner"] + "'";

                //var idd = Session["id_owner_enc"];
            }
            if (@Session["id_owner_organi"] != null)
            {
                command.CommandText = "update tb_owner_oragani set " +
                     "or_pass ='" + pass + "'" +
                     " where id_owner_organi = '" + @Session["id_owner_organi"] + "'";

                //var idd = Session["id_owner_enc"];
            }
            


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                TempData["chsus"] = "Yes";
               
                return RedirectToAction("changepass", "Home" , new { id = id_enc });
                
            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }

            



        }


        public ActionResult submitchpass_farmer()
        {
            string o_pass = Request.Form["o_pass"];
            string far_id = Request.Form["far_id"];
            var pass = GenerateSHA512String(o_pass);


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update tb_regfarmer set " +
                     "far_pass ='" + pass + "'" +
                     " where far_id = '" + far_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                TempData["chsus"] = "Yes";

                return RedirectToAction("changepassfarmer", "Home", new { id = Session["far_id_enc"] });

            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }





        }

        public ActionResult submitchpass_organi()
        {
            string o_pass = Request.Form["o_pass"];
            string far_id = Request.Form["far_id"];
            var pass = GenerateSHA512String(o_pass);


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update regis_organi set " +
                     "reg_password ='" + pass + "'" +
                     " where reg_organi_id = '" + far_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                TempData["chsus"] = "Yes";

                return RedirectToAction("changepassfarmer", "Home", new { id = Session["reg_organi_id_enc"] });

            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }





        }


        public ActionResult confrim_regis()
        {

            ViewBag.Message = "confrim_regis";

            return View("index");
        }



        public static string GenerateSHA512String(string input)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(input);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }




        public ActionResult UpdateRegister()
        {

            //ViewBag.Message = "updata_data";
            

            string o_name = Request.Form["o_name"];
            string o_surname = Request.Form["o_surname"];
            string o_idcard = Request.Form["o_idcard"];
            string o_age = Request.Form["o_age"];
            string o_career = Request.Form["o_career"];
            string o_address = Request.Form["o_address"];
            string o_road = Request.Form["o_road"];

            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string amp_id = Request.Form["amp_id"];
            string distr_id = Request.Form["distr_id"];

            string o_phone = Request.Form["o_phone"];
            string o_email = Request.Form["o_email"];
            string o_lineid = Request.Form["o_lineid"];

            string id_owner = Request.Form["id_owner"];


            string[] DateArray = o_age.Split('-');
            string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            if (PROVINCE_ID != "0") {
                command.CommandText = "update tb_ownerland set " +
                     "o_name ='" + o_name + "'," +
                     "o_surname= '" + o_surname + "'," +
                     "o_idcard= '" + o_idcard + "'," +
                     "o_age= '" + datebirth + "'," +
                     "o_career= '" + o_career + "'," +
                     "o_address= '" + o_address + "'," +
                     "o_road= '" + o_road + "'," +
                     "PROVINCE_ID= '" + PROVINCE_ID + "'," +
                     "AMPHUR_ID= '" + amp_id + "'," +
                     "DISTRICT_ID= '" + distr_id + "'," +
                     "o_phone= '" + o_phone + "'," +
                     "o_email= '" + o_email + "'," +
                     "o_lineid= '" + o_lineid + "'" +
                     " where id_owner = '" + id_owner + "'";
            }
            else
            {

                command.CommandText = "update tb_ownerland set " +
                     "o_name ='" + o_name + "'," +
                     "o_surname= '" + o_surname + "'," +
                     "o_idcard= '" + o_idcard + "'," +
                     "o_age= '" + datebirth + "'," +
                     "o_career= '" + o_career + "'," +
                     "o_address= '" + o_address + "'," +
                     "o_road= '" + o_road + "'," +
                     "o_phone= '" + o_phone + "'," +
                     "o_email= '" + o_email + "'," +
                     "o_lineid= '" + o_lineid + "'" +
                     " where id_owner = '" + id_owner + "'";

            }
            int i = command.ExecuteNonQuery();
            connection.Close();

            return RedirectToAction("data_owner", "Home");



        }


        public ActionResult loadprov()
        {
            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
            var province_id = Url.RequestContext.RouteData.Values["id"];

            //ViewBag.Message = show_province;
            ViewBag.Message = "provmain";

            // Response.Write("<script>alert('" + show_province + "')</script>");

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }
            ViewBag.PROVINCE_NAME = reader["PROVINCE_NAME"].ToString();


            reader.Close();

            //ViewData["province_id"] = province_id;
            ViewBag.province_id = province_id;
            return View("getaddr", ownerland);


        }

        public ActionResult loadaddr()
        {
            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
            var province_id = Url.RequestContext.RouteData.Values["id"];
            var datapro = Url.RequestContext.RouteData.Values["data"];
           

            //ViewBag.Message = show_province;
            //Response.Write(datapro);
           /* if (datapro == null)
            {
                ViewBag.Message = "prov";
            }

            if(datapro == "prov2")
            {
                ViewBag.Message = "prov2";
            }*/

            switch (datapro)
            {
                case null:
                    ViewBag.Message = "prov";
                    break;
                case "prov2":
                    ViewBag.Message = "prov2";
                    break;

            }

            // Response.Write("<script>alert('" + show_province + "')</script>");

             MySqlConnection connection = new MySqlConnection(constr);
             List<RegisterModel> ownerland = new List<RegisterModel>();
             connection.Open();

             MySqlCommand command = connection.CreateCommand();


                 command.CommandText = "select * from amphur where PROVINCE_ID='"+ province_id + "'";
                 MySqlDataReader reader = command.ExecuteReader();
                 while (reader.Read())
                 {

                     ownerland.Add(new RegisterModel
                     {
                         AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                         AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),

                     });
                 }

            

            reader.Close();

            return View("getaddr", ownerland);
            
            
        }

        public ActionResult loadtumbon()
        {
            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
            var aum_id = Url.RequestContext.RouteData.Values["id"];
            var datapro = Url.RequestContext.RouteData.Values["data"];

            //ViewBag.Message = show_province;
            //ViewBag.Message = "tum";

            switch (datapro)
            {
                case null:
                    ViewBag.Message = "tum";
                    break;
                case "aum2":
                    ViewBag.Message = "tum2";
                    break;

            }

            // Response.Write("<script>alert('" + show_province + "')</script>");

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select * from district where AMPHUR_ID='" + aum_id + "'";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),

                });
            }
            


            reader.Close();

            return View("getaddr", ownerland);


        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        

        public ActionResult login(int re)
        {
            ViewBag.Message = "login";

            if (re==1) { TempData["refle"] = re; }
            
            //Session["refresh"] = 1;

            return View("Index");
        }

        public ActionResult checklogin()
        {
            //string o_name = Request.Form["o_name"];
            // string o_surname = Request.Form["o_surname"];
            

            string typeloing = Request.Form["typeloing"];

            
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> login = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();


            if (typeloing == "1")
            {

                string o_email = Request.Form["o_email"];
                string o_pass = Request.Form["o_pass"];
                var ensc = GenerateSHA512String(o_pass);

                command.CommandText = "select *,count(*) as cc1 from tb_ownerland where o_email='" + o_email + "' and o_pass ='" + ensc + "' and o_confirm='2'";
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    int cc = Convert.ToInt32(reader["cc1"]);
                    if (cc > 0)
                    {
                        ViewBag.Message = "updata_data";
                        Session["id_owner_enc"] = CreatePassword(3) + reader["id_owner"] + CreatePassword(50);
                        Session["id_owner"] = Convert.ToInt32(reader["id_owner"]);
                        Session["o_name"] = reader["o_name"].ToString();
                        Session["o_surname"] = reader["o_surname"].ToString();
                        Session["o_email"] = reader["o_email"].ToString();

                        
                        TempData["cc"] = cc;
                        //return RedirectToAction("listland", "Home");
                        //return View("Index");
                    }
                }
                reader.Close();


                command2.CommandText = "select *,count(*) as cc2 from tb_owner_oragani where or_email='" + o_email + "' and or_pass ='" + ensc + "' and or_confirm='2'";
                MySqlDataReader reader22 = command2.ExecuteReader();

                if (reader22.Read())
                {
                    int cc2 = Convert.ToInt32(reader22["cc2"]);
                    if (cc2 > 0)
                    {
                        ViewBag.Message = "updata_data";
                        Session["id_owner_organi_enc"] = CreatePassword(3) + reader22["id_owner_organi"] + CreatePassword(50);
                        Session["id_owner_organi"] = Convert.ToInt32(reader22["id_owner_organi"]);
                        Session["or_name"] = reader22["or_name"].ToString();
                        Session["or_name_regis"] = reader22["or_name_regis"].ToString();
                        Session["or_email"] = reader22["or_email"].ToString();


                        TempData["cc"] = cc2;
                        //return RedirectToAction("listland", "Home");
                        //return View("Index");
                    }
                }
                reader22.Close();

                return RedirectToAction("redrirecttoow", "Home", new { id = 0 });
                //else
                //{
                //ViewData["nodata"] = "ไม่พบข้อมูล";
                //ViewBag.Message = "login";
                //    ViewBag.Alert = "NoData";
                //   Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                //return RedirectToAction("login", "Home");

                //   return View("Index");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scrips", "<script>alert('ไม่พบข้อมูล');</script>");
                // }
            }


            else if (typeloing == "2")
            {
                string o_email = Request.Form["o_email"];
                string o_pass = Request.Form["o_pass"];
                var ensc = GenerateSHA512String(o_pass);

                command.CommandText = "select *,count(*) as cc1 from tb_regfarmer where id_card='" + o_email + "' and far_pass ='" + ensc + "'";
                MySqlDataReader reader1 = command.ExecuteReader();

                

                if (reader1.Read())
                {
                    int cc = Convert.ToInt32(reader1["cc1"]);
                    if (cc > 0) {
                        Session["far_id_enc"] = CreatePassword(3) + reader1["far_id"] + CreatePassword(50);
                        Session["far_id"] = Convert.ToInt32(reader1["far_id"]);
                        Session["far_name"] = reader1["far_name"].ToString();
                        Session["far_surname"] = reader1["far_surname"].ToString();
                        Session["DISTRICT_ID"] = reader1["DISTRICT_ID"].ToString();
                        Session["far_tel"] = reader1["far_tel"].ToString();
                        Session["type_farmer_regis"] = 1;

                        //Session["cc"] = cc;
                        TempData["cc"] = cc;

                    }


                    //return RedirectToAction("farmersearch", "Home", new { id = 0 });
                    //return View("Index");
                }
                reader1.Close();


                command2.CommandText = "select *,count(*) as cc2 from regis_organi where reg_email='" + o_email + "' and reg_password ='" + ensc + "'";
                MySqlDataReader reader2 = command2.ExecuteReader();

                
                if (reader2.Read())
                {

                    int cc2 = Convert.ToInt32(reader2["cc2"]);
                    if (cc2 > 0)
                    {
                        Session["reg_organi_id_enc"] = CreatePassword(3) + reader2["reg_organi_id"] + CreatePassword(50);
                        Session["reg_organi_id"] = Convert.ToInt32(reader2["reg_organi_id"]);
                        Session["reg_name"] = reader2["reg_name"].ToString();
                        //Session["far_surname"] = reader["far_surname"].ToString();
                        Session["DISTRICT_ID"] = reader2["DISTRICT_ID"].ToString();
                        Session["reg_regis_tel"] = reader2["reg_regis_tel"].ToString();
                        Session["type_farmer_regis"] = 2;
                        //Session["cc2"] = cc2;
                        TempData["cc2"] = cc2;

                    }
                    

                    //return RedirectToAction("farmersearch", "Home", new { id = 0 });

                }
                reader2.Close();


                return RedirectToAction("redrirectto", "Home", new { id = 0 });
                /*  else
                  {
                      //ViewData["nodata"] = "ไม่พบข้อมูล";
                      ViewBag.Message = "login";
                      ViewBag.Alert = "NoData";
                      Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                      //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                      //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                      //return RedirectToAction("login", "Home");

                      return View("Index");
                      //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scrips", "<script>alert('ไม่พบข้อมูล');</script>");
                  }*/



            }
            else
            {

                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                //return RedirectToAction("login", "Home");

                return View("Index");

            }






                //return View("updata_data");
            }

        public ActionResult checklogin2()
        {
            //string o_name = Request.Form["o_name"];
            // string o_surname = Request.Form["o_surname"];


            string typeloing = Request.Form["typeloing"];


            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> login = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();

            string o_email = Request.Form["o_email"];
            string o_pass = Request.Form["o_pass"];
            var ensc = GenerateSHA512String(o_pass);

            if (typeloing == "1")
            {


               

                command.CommandText = "select *,count(*) as cc1 from tb_regfarmer where far_email='" + o_email + "' and far_pass ='" + ensc + "' and regis_confrim='2'";
                MySqlDataReader reader1 = command.ExecuteReader();



                if (reader1.Read())
                {
                    int cc = Convert.ToInt32(reader1["cc1"]);
                    if (cc > 0)
                    {
                        Session["far_id_enc"] = CreatePassword(3) + reader1["far_id"] + CreatePassword(50);
                        Session["far_id"] = Convert.ToInt32(reader1["far_id"]);
                        Session["far_name"] = reader1["far_name"].ToString();
                        Session["far_surname"] = reader1["far_surname"].ToString();
                        Session["DISTRICT_ID"] = reader1["DISTRICT_ID"].ToString();
                        Session["far_tel"] = reader1["far_tel"].ToString();
                        Session["type_farmer_regis"] = 1;
                        Session["type_status_regis"] = Convert.ToInt32(reader1["type_status_regis"]);

                        //Session["cc"] = cc;
                        TempData["cc"] = cc;

                    }

                    
                }
                reader1.Close();



                return RedirectToAction("redrirectto", "Home", new { id = 0 });
                
            }


            else if (typeloing == "2")
            {
               


                command2.CommandText = "select *,count(*) as cc2 from regis_organi where reg_email='" + o_email + "' and reg_password ='" + ensc + "' and reg_status='2'";
                MySqlDataReader reader2 = command2.ExecuteReader();


                if (reader2.Read())
                {

                    int cc2 = Convert.ToInt32(reader2["cc2"]);
                    if (cc2 > 0)
                    {
                        Session["reg_organi_id_enc"] = CreatePassword(3) + reader2["reg_organi_id"] + CreatePassword(50);
                        Session["reg_organi_id"] = Convert.ToInt32(reader2["reg_organi_id"]);
                        Session["reg_name"] = reader2["reg_name"].ToString();
                        //Session["far_surname"] = reader["far_surname"].ToString();
                        Session["DISTRICT_ID"] = reader2["DISTRICT_ID"].ToString();
                        Session["reg_regis_tel"] = reader2["reg_regis_tel"].ToString();
                        Session["type_farmer_regis"] = 2;
                        Session["type_status_regis_og"] = Convert.ToInt32(reader2["type_status_regis_og"]);
                        //Session["cc2"] = cc2;
                        TempData["cc2"] = cc2;

                    }


                    //return RedirectToAction("farmersearch", "Home", new { id = 0 });

                }
                reader2.Close();


                return RedirectToAction("redrirectto_og", "Home", new { id = 0 });
                /*  else
                  {
                      //ViewData["nodata"] = "ไม่พบข้อมูล";
                      ViewBag.Message = "login";
                      ViewBag.Alert = "NoData";
                      Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                      //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                      //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                      //return RedirectToAction("login", "Home");

                      return View("Index");
                      //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scrips", "<script>alert('ไม่พบข้อมูล');</script>");
                  }*/



            }
            else
            {

                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                //return RedirectToAction("login", "Home");

                return View("Index");

            }






            //return View("updata_data");
        }

        public ActionResult redrirectto()
        {
            string idlocaltion = Url.RequestContext.RouteData.Values["id"].ToString();


            //Response.Write("<script language=javascript>alert('"+ TempData["cc"] + "')</script>");
            //Response.Write("<script language=javascript>alert('" + TempData["cc2"] + "')</script>");
            if (TempData["cc"]==null && TempData["cc2"] == null) {

                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                return View("Index");

            }
            else
            {
                if (Convert.ToInt32(@Session["type_status_regis"])==1 || Convert.ToInt32(@Session["type_status_regis"]) == 3) {
                    return RedirectToAction("listland", "Home", new { id = 0 });
                }
                else if(Convert.ToInt32(@Session["type_status_regis"]) == 2) 
                {
                    return RedirectToAction("farmersearch", "Home", new { id = 0 });
                }
                else
                {

                    ViewBag.Message = "login";
                    ViewBag.Alert = "NoData";
                    Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                    return View("Index");
                }
                

                //return Content("xx"); ;
            }
           
                
        }

        public ActionResult redrirectto_og()
        {
            string idlocaltion = Url.RequestContext.RouteData.Values["id"].ToString();


            //Response.Write("<script language=javascript>alert('"+ TempData["cc"] + "')</script>");
            //Response.Write("<script language=javascript>alert('" + TempData["cc2"] + "')</script>");
            if (TempData["cc"] == null && TempData["cc2"] == null)
            {

                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                return View("Index");

            }
            else
            {
                if (Convert.ToInt32(@Session["type_status_regis_og"]) == 1 || Convert.ToInt32(@Session["type_status_regis_og"]) == 3)
                {
                    return RedirectToAction("listland", "Home", new { id = 0 });
                }
                else if (Convert.ToInt32(@Session["type_status_regis_og"]) == 2)
                {
                    return RedirectToAction("farmersearch", "Home", new { id = 0 });
                }
                else
                {

                    ViewBag.Message = "login";
                    ViewBag.Alert = "NoData";
                    Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                    return View("Index");
                }


                //return Content("xx"); ;
            }


        }
        
        public ActionResult redrirecttoow()
        {
            string idlocaltion = Url.RequestContext.RouteData.Values["id"].ToString();


            //Response.Write("<script language=javascript>alert('"+ TempData["cc"] + "')</script>");
            //Response.Write("<script language=javascript>alert('" + TempData["cc2"] + "')</script>");
            if (TempData["cc"] == null && TempData["cc2"] == null)
            {

                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                return View("Index");

            }
            else
            {
                return RedirectToAction("listland", "Home", new { id = 0 });

                //return Content("xx"); ;
            }


        }

        public ActionResult farmersearch()
        {
            

            string idlocaltion = Url.RequestContext.RouteData.Values["id"].ToString();


            ViewBag.Message = "farmer_data";


            MySqlConnection connection = new MySqlConnection(constr);

            List<RegisterModel> PROVINCE = new List<RegisterModel>();
            connection.Open();
            MySqlCommand command1 = connection.CreateCommand();
            command1.CommandText = "select * from province";
            MySqlDataReader reader1 = command1.ExecuteReader();

            while (reader1.Read())
            {

                PROVINCE.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader1["PROVINCE_ID"]),
                    PROVINCE_NAME = reader1["PROVINCE_NAME"].ToString(),

                });
            }
            reader1.Close();
            //TempData["PROVINCE"] = PROVINCE;
            ViewBag.PROVINCE = PROVINCE;
            ViewBag.idlocaltion = idlocaltion;

            /*if (Convert.ToInt32(Session["type_status_regis"])==1)
            {

            
            }*/
            return View("Index");
        }

        public ActionResult getsearch()
        {
            //var province_id = Url.RequestContext.RouteData.Values["id"];
            string[] DateArray = Url.RequestContext.RouteData.Values["id"].ToString().Split('-');
            string province_id = DateArray[0];
            string searchprovince = DateArray[1];
            string searchpaumpr = DateArray[2];
            string searchdis = DateArray[3];

            var aaa = Request["term"];
            MySqlConnection connection = new MySqlConnection(constr);

            List<RegisterModel> PROVINCE = new List<RegisterModel>();
            connection.Open();
            MySqlCommand command1 = connection.CreateCommand();

            if (searchprovince=="1") { 
                command1.CommandText = "select province.PROVINCE_ID,province.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME from province " +
                "inner join amphur as aa on(aa.PROVINCE_ID=province.PROVINCE_ID)" +
                "inner join district as dd on(aa.AMPHUR_ID=dd.AMPHUR_ID)" +
                "where province.PROVINCE_NAME like '%" + province_id + "%' or aa.AMPHUR_NAME like '%" + province_id + "%' or dd.DISTRICT_NAME like '%" + province_id + "%'" +
                "group by province.PROVINCE_ID";
            }

            if (searchpaumpr == "2")
            {
                command1.CommandText = "select province.PROVINCE_ID,province.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME from province " +
                "inner join amphur as aa on(aa.PROVINCE_ID=province.PROVINCE_ID)" +
                "inner join district as dd on(aa.AMPHUR_ID=dd.AMPHUR_ID)" +
                "where province.PROVINCE_NAME like '%" + province_id + "%' or aa.AMPHUR_NAME like '%" + province_id + "%' or dd.DISTRICT_NAME like '%" + province_id + "%'" +
                "group by aa.AMPHUR_ID";
            }

            if (searchdis == "3")
            {
                command1.CommandText = "select province.PROVINCE_ID,province.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME from province " +
                "inner join amphur as aa on(aa.PROVINCE_ID=province.PROVINCE_ID)" +
                "inner join district as dd on(aa.AMPHUR_ID=dd.AMPHUR_ID)" +
                "where province.PROVINCE_NAME like '%" + province_id + "%' or aa.AMPHUR_NAME like '%" + province_id + "%' or dd.DISTRICT_NAME like '%" + province_id + "%'";
            }
            MySqlDataReader reader1 = command1.ExecuteReader();

            while (reader1.Read())
            {

                PROVINCE.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader1["PROVINCE_ID"]),
                    PROVINCE_NAME = reader1["PROVINCE_NAME"].ToString(),
                    AMPHUR_ID = Convert.ToInt32(reader1["AMPHUR_ID"]),
                    AMPHUR_NAME = reader1["AMPHUR_NAME"].ToString(),
                    DISTRICT_ID = Convert.ToInt32(reader1["DISTRICT_ID"]),
                    DISTRICT_NAME = reader1["DISTRICT_NAME"].ToString(),

                });
            }
            reader1.Close();
            //TempData["PROVINCE"] = PROVINCE;
            ViewBag.PROVINCE = PROVINCE;
            ViewBag.searchprovince = searchprovince;
            ViewBag.searchpaumpr = searchpaumpr;
            ViewBag.searchdis = searchdis;
            return View("getsearch");
            //return Json(Newtonsoft.Json.JsonConvert.SerializeObject(PROVINCE), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getsearch2(string prefix)
        {
            //var province_id = Request["term"];
            var province_id = Url.RequestContext.RouteData.Values["id"];
            List<string> PROVINCE = new List<string>();
            //MySqlConnection conn = new MySqlConnection(constr);
            using (MySqlConnection conn = new MySqlConnection(constr))
            {
                //conn.ConnectionString = ConfigurationManager.ConnectionStrings["connectmysql"].ConnectionString;
                

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from province where PROVINCE_NAME like '%"+ prefix + "%'";
                    //cmd.Parameters.AddWithValue("@SearchText", prefix);
                    cmd.Connection = conn;
                    conn.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            PROVINCE.Add(string.Format("{0}-{1}", sdr["PROVINCE_NAME"], sdr["PROVINCE_ID"]));
                        }
                    }
                    conn.Close();
                }
            }
            return Json(PROVINCE, JsonRequestBehavior.AllowGet);
            //return PROVINCE.ToArray();
            //String[] PROVINCE = (String[])PROVINCE.ToArray(typeof(string));
            //string[] array = PROVINCE.ToArray();
            //return PROVINCE;
        }



    public ActionResult data_owner()
        {
            ViewBag.Message = "updata_data";
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> login = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select * from tb_ownerland as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where id_owner='" + Session["id_owner"] +"'";
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {


                string[] DateArray = reader["o_age"].ToString().Split('-');
                string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];

                ViewData["id_owner"] = Convert.ToInt32(reader["id_owner"]);
                ViewData["o_name"] = reader["o_name"].ToString();
                ViewData["o_surname"] = reader["o_surname"].ToString();
                ViewData["o_idcard"] = reader["o_idcard"].ToString();
                ViewData["o_age"] = datebirth;
                ViewData["o_career"] = reader["o_career"].ToString();
                ViewData["o_address"] = reader["o_address"].ToString();
                ViewData["o_road"] = reader["o_road"].ToString();
                ViewData["PROVINCE_ID"] = Convert.ToInt32(reader["PROVINCE_ID"]);
                ViewData["AMPHUR_ID"] = Convert.ToInt32(reader["AMPHUR_ID"]);
                ViewData["DISTRICT_ID"] = Convert.ToInt32(reader["DISTRICT_ID"]);
                ViewData["o_phone"] = reader["o_phone"].ToString();
                ViewData["o_email"] = reader["o_email"].ToString();
                ViewData["o_lineid"] = reader["o_lineid"].ToString();
                ViewData["o_date"] = reader["o_date"].ToString();

                ViewData["PROVINCE_NAME"] = reader["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader["DISTRICT_NAME"].ToString();


                reader.Close();
                //return RedirectToAction("Index", "Home");
                return View("Index");

            }
            else
            {

                 return View("Index");
            }
            

        }


        public ActionResult data_owner_organi()
        {
            ViewBag.Message = "data_owner_organi";
            MySqlConnection connection = new MySqlConnection(constr);


            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader2 = command.ExecuteReader();
            while (reader2.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader2["PROVINCE_ID"]),
                    PROVINCE_NAME = reader2["PROVINCE_NAME"].ToString(),

                });
            }


            reader2.Close();
            



            command.CommandText = "select * from tb_owner_oragani as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where id_owner_organi='" + Session["id_owner_organi"] + "'";
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {


               

                ViewData["id_owner_organi"] = Convert.ToInt32(reader["id_owner_organi"]);
                ViewData["or_name"] = reader["or_name"].ToString();
                ViewData["or_number"] = reader["or_number"].ToString();
                ViewData["or_name_regis"] = reader["or_name_regis"].ToString();
                ViewData["or_posi_regis"] = reader["or_posi_regis"].ToString();
                ViewData["or_address"] = reader["or_address"].ToString();
                ViewData["PROVINCE_NAME"] = reader["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader["AMPHUR_NAME"];
                ViewData["DISTRICT_NAME"] = reader["DISTRICT_NAME"];
                ViewData["or_phone_regis"] = reader["or_phone_regis"].ToString();
                ViewData["or_email"] = reader["or_email"].ToString();
               

                reader.Close();
                ViewBag.ownerland = ownerland;
                //return RedirectToAction("Index", "Home");
                return View("Index");

            }
            else
            {

                return View("Index");
            }


        }



        public ActionResult data_farmer()
        {
            ViewBag.Message = "updata_farmer";
            MySqlConnection connection = new MySqlConnection(constr);
            //List<RegisterFarnerModel> login = new List<RegisterFarnerModel>();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterFarnerModel> career = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> type_fatmer = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> r_career = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> addfar = new List<RegisterFarnerModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            List<RegisterFarnerModel> r_farmer_land_use = new List<RegisterFarnerModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (Session["far_id"] != null)
            {
                var keyfar_id = Session["far_id"];
                command.CommandText = "select * from tb_regfarmer as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where far_id='" + Session["far_id"] + "'";
            }
            else
            {
                var keyfar_id = Url.RequestContext.RouteData.Values["id"];
                command.CommandText = "select * from tb_regfarmer as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where far_id='" + keyfar_id + "'";
            }
            


            
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {


                string[] DateArray = reader["far_age"].ToString().Split('-');
                string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];
                int far_id = Convert.ToInt32(reader["far_id"]);
                ViewData["far_id"] = Convert.ToInt32(reader["far_id"]);
                ViewData["far_name"] = reader["far_name"].ToString();
                ViewData["far_surname"] = reader["far_surname"].ToString();
                ViewData["id_card"] = reader["id_card"].ToString();
                ViewData["far_age"] = datebirth;

                ViewData["age_day"] = DateArray[2];
                ViewData["age_month"] = DateArray[1];
                ViewData["age_year"] = DateArray[0];

                ViewData["PROVINCE_ID"] = Convert.ToInt32(reader["PROVINCE_ID"]);
                ViewData["AMPHUR_ID"] = Convert.ToInt32(reader["AMPHUR_ID"]);
                ViewData["DISTRICT_ID"] = Convert.ToInt32(reader["DISTRICT_ID"]);

                ViewData["PROVINCE_NAME"] = reader["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader["DISTRICT_NAME"].ToString();

                ViewData["far_tel"] = reader["far_tel"].ToString();
                ViewData["far_email"] = reader["far_email"].ToString();
                ViewData["far_line"] = reader["far_line"].ToString();
                ViewData["far_address"] = reader["far_address"].ToString();

                ViewData["far_road"] = reader["far_road"].ToString();
                ViewData["id_type_farmer"] = reader["id_type_farmer"].ToString();

               
                ViewData["wsize_land"] = reader["wsize_land"].ToString();
                ViewData["money_capital"] = reader["money_capital"].ToString();

                ViewData["type_status_regis"] = Convert.ToInt32(reader["type_status_regis"]);






                reader.Close();
                //return RedirectToAction("Index", "Home");



                command.CommandText = "select * from province";
                MySqlDataReader reader1 = command.ExecuteReader();
                while (reader1.Read())
                {

                    ownerland.Add(new RegisterModel
                    {
                        PROVINCE_ID = Convert.ToInt32(reader1["PROVINCE_ID"]),
                        PROVINCE_NAME = reader1["PROVINCE_NAME"].ToString(),

                    });
                }


                reader1.Close();


                command.CommandText = "select * from tb_career";
                MySqlDataReader reader2 = command.ExecuteReader();

                while (reader2.Read())
                {

                    career.Add(new RegisterFarnerModel
                    {
                        id_career = Convert.ToInt32(reader2["id_career"]),
                        career_name = reader2["career_name"].ToString(),
                        id_type_career = reader2["id_type_career"].ToString()

                    });
                }


                reader2.Close();

                command.CommandText = "select * from tb_type_farmer";
                MySqlDataReader reader3 = command.ExecuteReader();

                while (reader3.Read())
                {

                    type_fatmer.Add(new RegisterFarnerModel
                    {
                        id_type_farmer = Convert.ToInt32(reader3["id_type_farmer"]),
                        type_fatmer_name = reader3["type_fatmer_name"].ToString(),


                    });
                }


                reader3.Close();

                
                command.CommandText = "select * from r_career where far_id='"+ far_id + "'";
                MySqlDataReader reader4 = command.ExecuteReader();

                while (reader4.Read())
                {

                    r_career.Add(new RegisterFarnerModel
                    {
                        id_career = Convert.ToInt32(reader4["id_career"]),
                        //type_fatmer_name = reader3["type_fatmer_name"].ToString(),


                    });
                }


                reader4.Close();


                MySqlCommand command5 = connection.CreateCommand();
                command5.CommandText = "select * from tb_land_use where id_land_use in ('1','2','3')";
                MySqlDataReader reader5 = command5.ExecuteReader();

                while (reader5.Read())
                {

                    tb_land_use.Add(new RegisterModel
                    {
                        id_land_use = Convert.ToInt32(reader5["id_land_use"]),
                        land_use_name = reader5["land_use_name"].ToString()

                    });
                }


                reader5.Close();

                MySqlCommand command6 = connection.CreateCommand();
                command6.CommandText = "select * from r_farmer_land_use where far_id = '"+far_id+"'";
                MySqlDataReader reader6 = command6.ExecuteReader();

                while (reader6.Read())
                {

                    r_farmer_land_use.Add(new RegisterFarnerModel
                    {
                        farmer_user_land_id = Convert.ToInt32(reader6["farmer_user_land_id"]),
                        id_land_use = Convert.ToInt32(reader6["id_land_use"]),
                        far_id = reader6["far_id"].ToString(),
                        fdetail_user_land = reader6["fdetail_user_land"].ToString()

                    });
                }


                reader6.Close();

                ViewBag.tb_land_use = tb_land_use;
                ViewBag.r_farmer_land_use = r_farmer_land_use;




                ViewBag.ownerland = ownerland;
                ViewBag.career = career;
                ViewBag.type_fatmer = type_fatmer;
                ViewBag.r_career = r_career;



                return View("Index");

            }
            else
            {

                return View("Index");
            }


        }


        public ActionResult listland(string alert)
        {
            ViewBag.Message = "listland";

            
            
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            

           

            if (@Session["far_id"] != null)
            {
               
                command.CommandText = "select " +
               "ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,dd.img_name,ow.m_rent,ow.place_land,ow.status_land,ow.o_wland from owner_land as ow " +
               "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
               "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
               "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
               "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +


               "where ow.id_owner='" + Session["far_id"] + "' and type_owner_land='1'" +
               "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave";
            }
            else if (@Session["reg_organi_id"] != null)
            {
                command.CommandText = "select " +
               "ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,dd.img_name,ow.m_rent,ow.place_land,ow.status_land,ow.o_wland from owner_land as ow " +
               "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
               "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
               "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
               "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +


               "where ow.id_owner='" + @Session["reg_organi_id"] + "' and type_owner_land='2'" +
               "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave";
            }


            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    o_land_id = Convert.ToInt32(reader["o_land_id"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    dsave = reader["dsave"].ToString(),
                    img_name = reader["img_name"].ToString(),
                    m_rent = reader["m_rent"].ToString(),
                    place_land = reader["place_land"].ToString(),
                    status_land = reader["status_land"].ToString(),
                    o_wland = Convert.ToInt32(reader["o_wland"]),

                });
            }
            reader.Close();

            ViewBag.land = land;
            return View("Index");

        }

        public ActionResult fromland()
        {
            ViewBag.Message = "fromland";


            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterModel> type_land = new List<RegisterModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            List<RegisterModel> tb_water = new List<RegisterModel>();
            List<RegisterModel> land_group = new List<RegisterModel>();
            List<RegisterModel> condition_use_land = new List<RegisterModel>();

            
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }
            reader.Close();

            MySqlCommand command2 = connection.CreateCommand();
            command2.CommandText = "select * from type_land";
            MySqlDataReader reader2 = command2.ExecuteReader();

            while (reader2.Read())
            {

                type_land.Add(new RegisterModel
                {
                    type_land_id = Convert.ToInt32(reader2["type_land_id"]),
                    name_type = reader2["name_type"].ToString()

                });
            }

            
            reader2.Close();


            
            command.CommandText = "select * from tb_land_use";
            MySqlDataReader reader3 = command.ExecuteReader();

            while (reader3.Read())
            {

                tb_land_use.Add(new RegisterModel
                {
                    id_land_use = Convert.ToInt32(reader3["id_land_use"]),
                    land_use_name = reader3["land_use_name"].ToString()

                });
            }


            reader3.Close();


            command.CommandText = "select * from tb_water";
            MySqlDataReader reader4 = command.ExecuteReader();

            while (reader4.Read())
            {

                tb_water.Add(new RegisterModel
                {
                    id_data_water = Convert.ToInt32(reader4["id_data_water"]),
                    water_name = reader4["water_name"].ToString()

                });
            }


            reader4.Close();


            command.CommandText = "select * from land_group";
            MySqlDataReader reader5 = command.ExecuteReader();

            while (reader5.Read())
            {

                land_group.Add(new RegisterModel
                {
                    id_land_group = Convert.ToInt32(reader5["id_land_group"]),
                    land_group_name = reader5["land_group_name"].ToString(),
                    seq_num = Convert.ToInt32(reader5["seq_num"].ToString())

                });
            }


            reader5.Close();


            command.CommandText = "select * from condition_use_land order by condi_seq asc";
            MySqlDataReader reader6 = command.ExecuteReader();

            while (reader6.Read())
            {

                condition_use_land.Add(new RegisterModel
                {
                    id_condi_use_land = Convert.ToInt32(reader6["id_condi_use_land"]),
                    condi_name = reader6["condi_name"].ToString(),
                    condi_seq = Convert.ToInt32(reader6["condi_seq"]),

                });
            }


            reader6.Close();

            ViewBag.type_land = type_land;
            ViewBag.tb_land_use = tb_land_use;
            ViewBag.ownerland = ownerland;
            ViewBag.tb_water = tb_water;
            ViewBag.land_group = land_group;
            ViewBag.condition_use_land = condition_use_land;
            //ViewData["ownerland"] = ownerland;

            //mydata.ownerland;
            // mydata.type_land;
            return View("Index");

        }


        public ActionResult inserland(HttpPostedFileBase[] files,Int32[] id_land_use, string[] detail_user_land, Int32[] id_data_water, Int32[] id_land_group, Int32[] id_condi_use_land)
        {

            ViewBag.Message = "listland";
           // string[] lines = Regex.Split(files, "\r\n");

           

           
           
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string amp_id = Request.Form["amp_id"];
            string distr_id = Request.Form["distr_id"];

            string type_land_id = Request.Form["type_land_id"];
            string land_rai = Request.Form["land_rai"];
            string land_nga = Request.Form["land_nga"];
            string land_va = Request.Form["land_va"];
            string m_rent = Request.Form["m_rent"];
            string period_per = Request.Form["period_per"];
            string period = Request.Form["period"];
            string codition = Request.Form["codition"];
            string id_owner = Request.Form["id_owner"];
            string latitude_land = Request.Form["latitude_land"];
            string longitude_land = Request.Form["longitude_land"];
            string place_land = Request.Form["place_land"];
            string o_land_num = Request.Form["o_land_num"];

            string o_wland = Request.Form["o_wland"];
            string o_road = Request.Form["o_road"];
            string type_owner_land = Request.Form["type_owner_land"];


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            
            command.CommandText = "insert into owner_land " +
                "(PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "type_land_id," +
                "land_rai," +
                "land_nga," +
                "land_va," +
                "m_rent," +
                "period_per," +
                "period," +
                "codition," +
                "id_owner," +
                "latitude_land," +
                "longitude_land," +
                "place_land," +
                "o_land_num," +
                "o_wland," +
                "o_road," +
                "type_owner_land" +
                ")" +
                "VALUES " +
                "('" + PROVINCE_ID + "'," +
                "'" + amp_id + "'," +
                "'" + distr_id + "'," +
                "'" + type_land_id + "'," +
                "'" + land_rai + "'," +
                "'" + land_nga + "'," +
                "'" + land_va + "'," +
                "'" + m_rent + "'," +
                "'" + period_per + "'," +
                "'" + period + "'," +
                "'" + codition + "'," +
                "'" + id_owner + "'," +
                "'" + latitude_land + "'," +
                "'" + longitude_land + "'," +
                "'" + place_land + "'," +
                "'" + o_land_num + "'," +
                "'" + o_wland + "'," +
                "'" + o_road + "'," +
                "'" + type_owner_land + "'" +
                ")";
            
            int i = command.ExecuteNonQuery();
            long o_land_id = command.LastInsertedId;
            connection.Close();

            if (i>0) {
                

                int r=1;

                 foreach (HttpPostedFileBase file in files)
                 {

                     //Checking file is available to save.  
                     if (file != null)
                     {

                        connection.Open();

                        //var ID = "xxxxxxxxxxx";
                        var rantext = CreatePassword(5);
                        MySqlCommand command2 = connection.CreateCommand();
                        //var ID = (int)command.ExecuteScalar();
                        var ext = Path.GetExtension(file.FileName);

                        command2.CommandText = "insert into img_land " +
                            "(img_name," +
                            "id_owner," +
                            "o_land_id" +
                            ")" +
                            "VALUES " +
                            "('" + o_land_id + "-"+ r + rantext + ext+ "'," +
                            "'" + id_owner + "'," +
                            "'" + o_land_id + "'" +


                            ")";
                        int i2 = command2.ExecuteNonQuery();
                        long img_id = command2.LastInsertedId;
                        
                        connection.Close();
                        
                        

                         //var InputFileName = Path.GetFileName(file.FileName);
                         
                         //var ServerSavePath = Path.Combine(Server.MapPath("~/imgland/") + InputFileName);
                         string ServerSavePath = Server.MapPath("~/imgland/" + o_land_id + "-"+r+ rantext + ext);
                         //Save file to server folder  
                         file.SaveAs(ServerSavePath);
                         //assigning file uploaded status to ViewBag for showing message to user.  
                         ViewBag.UploadStatus = files.Count().ToString() + " files uploaded successfully.";
                     }
                    r++;
                }

               // int aa = 1;
                //foreach (Int32 id_land_use1 in id_land_use)
                int idd = id_land_use.Count()-1;
                
                connection.Open();
                if (idd >=0) { 
                
             
                
                //id_land_use = Request.Form.GetValues("id_land_use");
                string[] ids = Request.Form.GetValues("id_land_use");

                //condi_detail = Request.Form.GetValues("condi_detail");
               

                
                for (i=0;i<=idd;i++)
                {
                    string[] detail_user_land2 = Request.Form.GetValues("detail_user_land");
                    
                    MySqlCommand command33 = connection.CreateCommand();
                    //if (id_land_use[i] == 2 && id_land_use[i] ==3 && detail_user_land[i] != "") {
                    //if (id_land_use[i] >0 && detail_user_land[i] != "")
                    //{
                        command33.CommandText = "insert into r_owner_land_use " +
                        "(id_land_use," +
                        "o_land_id," +
                         "detail_user_land" +

                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + id_land_use[i] + "'," +
                        "'" + o_land_id + "'," +
                        "'" + detail_user_land2[i] + "'" +


                        ")";

                        int i33 = command33.ExecuteNonQuery();
                    //}
                    


                    
                    //}  

                }
                }



                foreach (Int32 id_data_water1 in id_data_water)
                {
                    
                    MySqlCommand command2 = connection.CreateCommand();

                    

                        command2.CommandText = "insert into r_land_water " +
                            "(id_data_water," +
                            "o_land_id" +
                             

                            ")" +
                            "VALUES " +
                            "(" +

                            "'" + id_data_water1 + "'," +
                            "'" + o_land_id + "'" +
                          


                            ")";


                        int i2 = command2.ExecuteNonQuery();
                   
                }


                string land_group_detail = Request.Form["land_group_detail"];
                foreach (Int32 id_land_group1 in id_land_group)
                {
                    
                    MySqlCommand command2 = connection.CreateCommand();



                    command2.CommandText = "insert into r_land_group " +
                        "(id_land_group," +
                        "o_land_id," +
                        "land_group_detail" +


                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + id_land_group1 + "'," +
                        "'" + o_land_id + "'," +
                        "'" + land_group_detail + "'" +



                        ")";


                    int i2 = command2.ExecuteNonQuery();
                    
                }

               // int aaaa = Convert.ToInt32(id_condi_use_land);

                if (id_condi_use_land != null) {
                int idd2 = id_condi_use_land.Count() - 1;
                string condi_detail = Request.Form["condi_detail"];
                string[] ids2 = Request.Form.GetValues("id_condi_use_land");

                for (i = 0; i <= idd2; i++)
                {
                    MySqlCommand command3 = connection.CreateCommand();
                    //if (id_land_use[i] == 2 && id_land_use[i] ==3 && detail_user_land[i] != "") {
                    if (id_condi_use_land[i] > 0)
                    {
                        command3.CommandText = "insert into r_condi_land " +
                        "(id_condi_use_land," +
                        "o_land_id," +
                         "condi_detail" +

                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + ids2[i] + "'," +
                        "'" + o_land_id + "'," +
                        "'" + condi_detail + "'" +


                        ")";
                        int i3 = command3.ExecuteNonQuery();
                    }
                    //}  

                }
                }

                connection.Close();


                Session["alert"] = "Yes";
                return RedirectToAction("listland", "Home", new { alert = "Yes" });
               
            }
            else
            {
                
                Response.Write("<script language=javascript>alert('บันทึกไม่สำเร็จ');</script>");
                return View("index");
            }
        }

        public ActionResult showland(int id)
        {
            ViewBag.Message = "showland";
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterModel> type_land = new List<RegisterModel>();

            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            List<RegisterModel> tb_water = new List<RegisterModel>();
            List<RegisterModel> land_group = new List<RegisterModel>();
            List<RegisterModel> condition_use_land = new List<RegisterModel>();

            List<RegisterModel> r_owner_land_use = new List<RegisterModel>();
            List<RegisterModel> r_land_water = new List<RegisterModel>();
            List<RegisterModel> r_land_group = new List<RegisterModel>();
            List<RegisterModel> r_condi_land = new List<RegisterModel>();


            connection.Open();

            MySqlCommand command = connection.CreateCommand();



            command.CommandText = "select " +
               "ow.type_owner_land,ow.o_road,ow.o_wland,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,dd.img_name,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land,ow.status_land,ow.o_land_num from owner_land as ow " +
               "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
               "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
               "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
               "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +
              // "where ow.id_owner='" + Session["id_owner"] + "' and ow.o_land_id='" + id + "'";
              "where ow.o_land_id='" + id + "'";

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {



                ViewData["o_land_id"] = Convert.ToInt32(reader["o_land_id"]);
                ViewData["PROVINCE_NAME"] = reader["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader["DISTRICT_NAME"].ToString();
                ViewData["dsave"] = reader["dsave"].ToString();
                ViewData["m_rent"] = reader["m_rent"].ToString();
                ViewData["place_land"] = reader["place_land"].ToString();
                ViewData["type_land_id"] = reader["type_land_id"].ToString();
                ViewData["land_rai"] = reader["land_rai"].ToString();
                ViewData["land_nga"] = reader["land_nga"].ToString();
                ViewData["land_va"] = reader["land_va"].ToString();
                ViewData["period_per"] = reader["period_per"].ToString();
                ViewData["period"] = reader["period"].ToString();
                ViewData["codition"] = reader["codition"].ToString();
                ViewData["latitude_land"] = reader["latitude_land"].ToString();
                ViewData["longitude_land"] = reader["longitude_land"].ToString();
                ViewData["place_land"] = reader["place_land"].ToString();
                ViewData["status_land"] = Convert.ToInt32(reader["status_land"]);
                ViewData["o_land_num"] = reader["o_land_num"].ToString();
                ViewData["o_wland"] = reader["o_wland"].ToString();
                ViewData["o_road"] = Convert.ToInt32(reader["o_road"].ToString());
                ViewData["type_owner_land"] = Convert.ToInt32(reader["type_owner_land"].ToString());

                reader.Close();


                MySqlCommand command1 = connection.CreateCommand();
                command1.CommandText = "select * from province";
                MySqlDataReader reader1 = command1.ExecuteReader();

                while (reader1.Read())
                {

                    ownerland.Add(new RegisterModel
                    {
                        PROVINCE_ID = Convert.ToInt32(reader1["PROVINCE_ID"]),
                        PROVINCE_NAME = reader1["PROVINCE_NAME"].ToString(),

                    });
                }
                reader1.Close();

                MySqlCommand command2 = connection.CreateCommand();
                command2.CommandText = "select * from type_land";
                MySqlDataReader reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {

                    type_land.Add(new RegisterModel
                    {
                        type_land_id = Convert.ToInt32(reader2["type_land_id"]),
                        name_type = reader2["name_type"].ToString()

                    });
                }


                reader2.Close();


                command.CommandText = "select * from tb_land_use";
                MySqlDataReader reader3 = command.ExecuteReader();

                while (reader3.Read())
                {

                    tb_land_use.Add(new RegisterModel
                    {
                        id_land_use = Convert.ToInt32(reader3["id_land_use"]),
                        land_use_name = reader3["land_use_name"].ToString()

                    });
                }


                reader3.Close();

                command.CommandText = "select * from r_owner_land_use where o_land_id ='" + id + "'";
                MySqlDataReader reader33 = command.ExecuteReader();

                while (reader33.Read())
                {

                    r_owner_land_use.Add(new RegisterModel
                    {
                        owner_user_land_id = Convert.ToInt32(reader33["owner_user_land_id"]),
                        id_land_use = Convert.ToInt32(reader33["id_land_use"].ToString()),
                        detail_user_land = reader33["detail_user_land"].ToString()

                    });
                }


                reader33.Close();


                command.CommandText = "select * from tb_water";
                MySqlDataReader reader4 = command.ExecuteReader();

                while (reader4.Read())
                {

                    tb_water.Add(new RegisterModel
                    {
                        id_data_water = Convert.ToInt32(reader4["id_data_water"]),
                        water_name = reader4["water_name"].ToString()

                    });
                }


                reader4.Close();

                command.CommandText = "select * from r_land_water where o_land_id ='" + id + "'";
                MySqlDataReader reader44 = command.ExecuteReader();

                while (reader44.Read())
                {

                    r_land_water.Add(new RegisterModel
                    {
                        id_land_water = Convert.ToInt32(reader44["id_land_water"]),
                        id_data_water = Convert.ToInt32(reader44["id_data_water"].ToString())
                       

                    });
                }


                reader44.Close();


                command.CommandText = "select * from land_group";
                MySqlDataReader reader5 = command.ExecuteReader();

                while (reader5.Read())
                {

                    land_group.Add(new RegisterModel
                    {
                        id_land_group = Convert.ToInt32(reader5["id_land_group"]),
                        land_group_name = reader5["land_group_name"].ToString(),
                        seq_num = Convert.ToInt32(reader5["seq_num"].ToString())

                    });
                }


                reader5.Close();

                command.CommandText = "select * from r_land_group where o_land_id ='" + id + "'";
                MySqlDataReader reader55 = command.ExecuteReader();

                while (reader55.Read())
                {

                    r_land_group.Add(new RegisterModel
                    {
                        id_r_land_group = Convert.ToInt32(reader55["id_r_land_group"]),
                        id_land_group = Convert.ToInt32(reader55["id_land_group"]),
                        o_land_id = Convert.ToInt32(reader55["o_land_id"].ToString()),
                        land_group_detail = reader55["land_group_detail"].ToString(),

                    });
                }


                reader55.Close();


                command.CommandText = "select * from condition_use_land";
                MySqlDataReader reader6 = command.ExecuteReader();

                while (reader6.Read())
                {

                    condition_use_land.Add(new RegisterModel
                    {
                        id_condi_use_land = Convert.ToInt32(reader6["id_condi_use_land"]),
                        condi_name = reader6["condi_name"].ToString()

                    });
                }


                reader6.Close();

                command.CommandText = "select * from r_condi_land where o_land_id ='" + id + "'";
                MySqlDataReader reader66 = command.ExecuteReader();

                while (reader66.Read())
                {

                    r_condi_land.Add(new RegisterModel
                    {
                        id_r_condi_land = Convert.ToInt32(reader66["id_r_condi_land"]),
                        id_condi_use_land = Convert.ToInt32(reader66["id_condi_use_land"]),
                        condi_detail = reader66["condi_detail"].ToString(),
                        o_land_id = Convert.ToInt32(reader66["o_land_id"]),
                    });
                }


                reader66.Close();

                ViewBag.type_land = type_land;
                ViewBag.tb_land_use = tb_land_use;
                ViewBag.ownerland = ownerland;
                ViewBag.tb_water = tb_water;
                ViewBag.land_group = land_group;
                ViewBag.condition_use_land = condition_use_land;

                ViewBag.r_owner_land_use = r_owner_land_use;
                ViewBag.r_land_water = r_land_water;
                ViewBag.r_land_group = r_land_group;
                ViewBag.r_condi_land = r_condi_land;




                //return RedirectToAction("Index", "Home");
                return View("Index");

            }
            else
            {

                return View("Index");
            }
        }
    public ActionResult updateland(Int32[] id_land_use, string[] detail_user_land, Int32[] id_data_water, Int32[] id_land_group, Int32[] id_condi_use_land)
        {

            ViewBag.Message = "listland";
            

            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string amp_id = Request.Form["amp_id"];
            string distr_id = Request.Form["distr_id"];

            string type_land_id = Request.Form["type_land_id"];
            string land_rai = Request.Form["land_rai"];
            string land_nga = Request.Form["land_nga"];
            string land_va = Request.Form["land_va"];
            string m_rent = Request.Form["m_rent"];
            string period_per = Request.Form["period_per"];
            string period = Request.Form["period"];
            string codition = Request.Form["codition"];
            string id_owner = Request.Form["id_owner"];
            string latitude_land = Request.Form["latitude_land"];
            string longitude_land = Request.Form["longitude_land"]; 
            string place_land = Request.Form["place_land"];

            string geto_land_id = Request.Form["o_land_id"];

            string status_land = Request.Form["status_land"];

            string o_land_num = Request.Form["o_land_num"];
            string o_wland = Request.Form["o_wland"];
            string o_road = Request.Form["o_road"];
            string type_owner_land01 = Request.Form["type_owner_land"];

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (PROVINCE_ID != "0")
            {
                command.CommandText = "update owner_land set " +
                    "PROVINCE_ID='" + PROVINCE_ID + "'," +
                    "AMPHUR_ID='" + amp_id + "'," +
                    "DISTRICT_ID='" + distr_id + "'," +
                    "type_land_id='" + type_land_id + "'," +
                    "land_rai='" + land_rai + "'," +
                    "land_nga='" + land_nga + "'," +
                    "land_va='" + land_va + "'," +
                    "m_rent='" + m_rent + "'," +
                    "period_per='" + period_per + "'," +
                    "period='" + period + "'," +
                    "o_road='" + o_road + "'," +
                    "codition='" + codition + "'," +
                    //"latitude_land='" + latitude_land + "'," +
                    //"longitude_land='" + longitude_land + "'," +
                    "place_land='" + place_land + "'," +
                    "status_land='" + status_land + "'," +
                    "o_wland='" + o_wland + "'," +
                    "o_land_num='" + o_land_num + "'" +
                    "where o_land_id='" + geto_land_id + "'";

            }
            else
            {
                command.CommandText = "update owner_land set " +
                    "type_land_id='" + type_land_id + "'," +
                    "land_rai='" + land_rai + "'," +
                    "land_nga='" + land_nga + "'," +
                    "land_va='" + land_va + "'," +
                    "m_rent='" + m_rent + "'," +
                    "period_per='" + period_per + "'," +
                    "period='" + period + "'," +
                    "o_road='" + o_road + "'," +
                    "codition='" + codition + "'," +
                    //"latitude_land='" + latitude_land + "'," +
                    //"longitude_land='" + longitude_land + "'," +
                    "place_land='" + place_land + "'," +
                    "status_land='" + status_land + "'," +
                    "o_land_num='" + o_land_num + "'," +
                    "o_wland='" + o_wland + "'" +
                    "where o_land_id='" + geto_land_id + "'";
            }



            int i = command.ExecuteNonQuery();
            
            connection.Close();

            if (i > 0)
            {


                int r = 1;
                Session["alert"] = "Yes";

                connection.Open();
                command.CommandText = "delete from r_condi_land where o_land_id='" + geto_land_id + "'";
                int deli = command.ExecuteNonQuery();

                command.CommandText = "delete from r_land_group where o_land_id='" + geto_land_id + "'";
                int deli2 = command.ExecuteNonQuery();

                command.CommandText = "delete from r_land_water where o_land_id='" + geto_land_id + "'";
                int deli3 = command.ExecuteNonQuery();

                command.CommandText = "delete from r_owner_land_use where o_land_id='" + geto_land_id + "'";
                int deli4 = command.ExecuteNonQuery();
                connection.Close();
                // int aa = 1;
                //foreach (Int32 id_land_use1 in id_land_use)
                int idd = id_land_use.Count() - 1;
               // int idd2 = id_condi_use_land.Count() - 1;


                //id_land_use = Request.Form.GetValues("id_land_use");
                string[] ids = Request.Form.GetValues("id_land_use");

                //condi_detail = Request.Form.GetValues("condi_detail");
                
               // string[] ids2 = Request.Form.GetValues("id_condi_use_land");

                connection.Open();
                for (i = 0; i <= idd; i++)
                {
                    string[] detail_user_land2 = Request.Form.GetValues("detail_user_land");

                    MySqlCommand command33 = connection.CreateCommand();
                    //if (id_land_use[i] == 2 && id_land_use[i] ==3 && detail_user_land[i] != "") {
                    //if (id_land_use[i] >0 && detail_user_land[i] != "")
                    //{
                    command33.CommandText = "insert into r_owner_land_use " +
                    "(id_land_use," +
                    "o_land_id," +
                     "detail_user_land" +

                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + id_land_use[i] + "'," +
                    "'" + geto_land_id + "'," +
                    "'" + detail_user_land2[i] + "'" +


                    ")";

                    int i33 = command33.ExecuteNonQuery();
                    //}




                    //}  

                }




                foreach (Int32 id_data_water1 in id_data_water)
                {

                    MySqlCommand command2 = connection.CreateCommand();



                    command2.CommandText = "insert into r_land_water " +
                        "(id_data_water," +
                        "o_land_id" +


                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + id_data_water1 + "'," +
                        "'" + geto_land_id + "'" +



                        ")";


                    int i2 = command2.ExecuteNonQuery();

                }


                string land_group_detail = Request.Form["land_group_detail"];
                foreach (Int32 id_land_group1 in id_land_group)
                {

                    MySqlCommand command2 = connection.CreateCommand();



                    command2.CommandText = "insert into r_land_group " +
                        "(id_land_group," +
                        "o_land_id," +
                        "land_group_detail" +


                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + id_land_group1 + "'," +
                        "'" + geto_land_id + "'," +
                        "'" + land_group_detail + "'" +



                        ")";


                    int i2 = command2.ExecuteNonQuery();

                }



                if (id_condi_use_land != null && o_wland == "1")
                {
                    int idd2 = id_condi_use_land.Count() - 1;
                    string condi_detail = Request.Form["condi_detail"];
                    string[] ids2 = Request.Form.GetValues("id_condi_use_land");

                    for (i = 0; i <= idd2; i++)
                    {
                        MySqlCommand command3 = connection.CreateCommand();
                        //if (id_land_use[i] == 2 && id_land_use[i] ==3 && detail_user_land[i] != "") {
                        if (id_condi_use_land[i] > 0)
                        {
                            command3.CommandText = "insert into r_condi_land " +
                            "(id_condi_use_land," +
                            "o_land_id," +
                             "condi_detail" +

                            ")" +
                            "VALUES " +
                            "(" +

                            "'" + ids2[i] + "'," +
                            "'" + geto_land_id + "'," +
                            "'" + condi_detail + "'" +


                            ")";
                            int i3 = command3.ExecuteNonQuery();
                        }
                        //}  

                    }
                }
                connection.Close();


                /*if (Session["id_owner"] != null)
                {
                    return RedirectToAction("listland", "Home", new { alert = "Yes" });

                }*/
                if (Session["far_id"] != null)
                {
                    return RedirectToAction("listland", "Home", new { alert = "Yes" });

                }
                else if (Session["reg_organi_id"] != null)
                {
                    return RedirectToAction("listland", "Home", new { alert = "Yes" });

                }
                else if (Session["staff_id"] != null)
                {
                    return RedirectToAction("landOwner", "Backend", new { type_owner_land = type_owner_land01 });
                }
                else
                {
                    Response.Write("<script language=javascript>alert('คุณไม่ได้รับอนุญาต');</script>");
                    return View("index");
                }


               

            }
            else
            {

                Response.Write("<script language=javascript>alert('บันทึกไม่สำเร็จ');</script>");
                return View("index");
            }
        }

        public ActionResult formimg(int o_land_id,string PROVINCE_NAME, string AMPHUR_NAME, string DISTRICT_NAME, string type_owner_land)
        {
            
            List<RegisterModel> imgland = new List<RegisterModel>();
            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command1 = connection.CreateCommand();
            command1.CommandText = "select * from img_land where o_land_id='"+ o_land_id + "'";
            MySqlDataReader reader1 = command1.ExecuteReader();

            while (reader1.Read())
            {

                imgland.Add(new RegisterModel
                {
                    img_id = Convert.ToInt32(reader1["img_id"]),
                    img_name = reader1["img_name"].ToString(),

                });
            }
            reader1.Close();


            ViewBag.Message = "formimg";
            ViewBag.o_land_id = o_land_id;
            ViewBag.imgland = imgland;
            ViewBag.PROVINCE_NAME = PROVINCE_NAME;
            ViewBag.AMPHUR_NAME = AMPHUR_NAME;
            ViewBag.DISTRICT_NAME = DISTRICT_NAME;
            ViewBag.type_owner_land = type_owner_land;

            return View("Index");
        }

        public ActionResult inserimg(HttpPostedFileBase[] files)
        {
            int r = 1;
            string o_land_id1 = Request.Form["o_land_id"];
            string id_owner = Request.Form["id_owner"];

            string PROVINCE_NAME1 = Request.Form["vPROVINCE_NAME"];
            string AMPHUR_NAME1 = Request.Form["vAMPHUR_NAME"];
            string DISTRICT_NAME1 = Request.Form["vDISTRICT_NAME"];

            string status_land = Request.Form["status_land"];
            
            MySqlConnection connection = new MySqlConnection(constr);
            foreach (HttpPostedFileBase file in files)
            {

                //Checking file is available to save.  
                if (file != null)
                {

                    connection.Open();

                    //var ID = "xxxxxxxxxxx";
                    var rantext = CreatePassword(5);
                    MySqlCommand command2 = connection.CreateCommand();
                    //var ID = (int)command.ExecuteScalar();
                    var ext = Path.GetExtension(file.FileName);

                    command2.CommandText = "insert into img_land " +
                        "(img_name," +
                        "id_owner," +
                        "o_land_id" +
                        ")" +
                        "VALUES " +
                        "('" + o_land_id1 + "-" + r + rantext + ext + "'," +
                        "'" + id_owner + "'," +
                        "'" + o_land_id1 + "'" +


                        ")";
                    int i2 = command2.ExecuteNonQuery();
                    long img_id = command2.LastInsertedId;


                    MySqlCommand command = connection.CreateCommand();
                    command.CommandText = "update owner_land set " +
                                        
                                        "status_land='" + status_land + "'" +
                                       
                                        "where o_land_id='" + o_land_id1 + "'";

                    int i = command.ExecuteNonQuery();

                    connection.Close();



                    //var InputFileName = Path.GetFileName(file.FileName);

                    //var ServerSavePath = Path.Combine(Server.MapPath("~/imgland/") + InputFileName);
                    string ServerSavePath = Server.MapPath("~/imgland/" + o_land_id1 + "-" + r + rantext + ext);
                    //Save file to server folder  
                    file.SaveAs(ServerSavePath);
                    //assigning file uploaded status to ViewBag for showing message to user.  
                    ViewBag.UploadStatus = files.Count().ToString() + " files uploaded successfully.";
                }
                r++;
            }

            ViewBag.Message = "formimg";
           

            return RedirectToAction("formimg", "Home", new { alert = "Yes", o_land_id= o_land_id1, PROVINCE_NAME= PROVINCE_NAME1, AMPHUR_NAME= AMPHUR_NAME1, DISTRICT_NAME= DISTRICT_NAME1 });

           
        }

        public ActionResult delimg(int img_id, string img_name, int o_land_id1, string PROVINCE_NAME1, string AMPHUR_NAME1, string DISTRICT_NAME1)
        {

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "delete from img_land where img_id='" + img_id + "'";

            
            FileInfo FileIn = new FileInfo(Server.MapPath("~/imgland/"+img_name));
            if (FileIn.Exists)
            {
                FileIn.Delete();
                int i = command.ExecuteNonQuery();
            }




            connection.Close();


            ViewBag.Message = "formimg";
           

            return RedirectToAction("formimg", "Home", new { alert = "Yes", o_land_id = o_land_id1 , PROVINCE_NAME = PROVINCE_NAME1, AMPHUR_NAME = AMPHUR_NAME1, DISTRICT_NAME = DISTRICT_NAME1 });
        }

        public ActionResult delland(int id)
        {

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "delete from img_land where o_land_id='" + id + "'";
            int deli1 = command.ExecuteNonQuery();

            command.CommandText = "delete from r_condi_land where o_land_id='" + id + "'";
            int deli = command.ExecuteNonQuery();

            command.CommandText = "delete from r_land_group where o_land_id='" + id + "'";
            int deli2 = command.ExecuteNonQuery();

            command.CommandText = "delete from r_land_water where o_land_id='" + id + "'";
            int deli3 = command.ExecuteNonQuery();

            command.CommandText = "delete from r_owner_land_use where o_land_id='" + id + "'";
            int deli4 = command.ExecuteNonQuery();


            command.CommandText = "delete from owner_land where o_land_id='" + id + "'";
            int deli5 = command.ExecuteNonQuery();

            connection.Close();


           /* FileInfo FileIn = new FileInfo(Server.MapPath("~/imgland/" + img_name));
            if (FileIn.Exists)
            {
                FileIn.Delete();
                int i = command.ExecuteNonQuery();
            }*/


            connection.Close();


            //ViewBag.Message = "formimg";


            return RedirectToAction("listland", "Home", new { id = 0 });
        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public string capchar(int length)
        {
            const string valid = "1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }




        public ActionResult check_idcard()
        {
           
            var o_idcard = Url.RequestContext.RouteData.Values["id"];

            
            ViewBag.Message = "chkid";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> owner = new List<RegisterModel>();
            

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select count(*) as crow from tb_ownerland where o_idcard='" + o_idcard + "'";

            connection.Open();
            //MySqlDataReader reader = command.ExecuteReader();
            var NumRows = (long)command.ExecuteScalar();
            connection.Close();
            
            

            if (NumRows>=1) {

                return Content("1");
            }
            else
            {
                return Content("0");

            }

            

        }

        public ActionResult check_idcard_fatmer()
        {

            var id_card = Url.RequestContext.RouteData.Values["id"];


            ViewBag.Message = "chkid";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> owner = new List<RegisterModel>();


            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select count(*) as crow from tb_regfarmer where id_card='" + id_card + "'";

            connection.Open();
            //MySqlDataReader reader = command.ExecuteReader();
            var NumRows = (long)command.ExecuteScalar();
            connection.Close();



            if (NumRows >= 1)
            {

                return Content("1");
            }
            else
            {
                return Content("0");

            }



        }




        public ActionResult regisfa(Int32 id, Int32 type)
        {



            ViewBag.Message = "regisfa";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterFarnerModel> career = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> type_fatmer = new List<RegisterFarnerModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }


            reader.Close();


            MySqlCommand command2 = connection.CreateCommand();
            command2.CommandText = "select * from tb_career";
            MySqlDataReader reader2 = command2.ExecuteReader();

            while (reader2.Read())
            {

                career.Add(new RegisterFarnerModel
                {
                    id_career = Convert.ToInt32(reader2["id_career"]),
                    career_name = reader2["career_name"].ToString(),
                    id_type_career = reader2["id_type_career"].ToString()

                });
            }


            reader2.Close();


            MySqlCommand command3 = connection.CreateCommand();
            command3.CommandText = "select * from tb_type_farmer";
            MySqlDataReader reader3 = command3.ExecuteReader();

            while (reader3.Read())
            {

                type_fatmer.Add(new RegisterFarnerModel
                {
                    id_type_farmer = Convert.ToInt32(reader3["id_type_farmer"]),
                    type_fatmer_name = reader3["type_fatmer_name"].ToString(),
               

                });
            }


            reader3.Close();


            MySqlCommand command4 = connection.CreateCommand();
            command4.CommandText = "select * from tb_land_use where id_land_use in ('1','2','3','6')";
            MySqlDataReader reader4 = command4.ExecuteReader();

            while (reader4.Read())
            {

                tb_land_use.Add(new RegisterModel
                {
                    id_land_use = Convert.ToInt32(reader4["id_land_use"]),
                    land_use_name = reader4["land_use_name"].ToString(),
                    land_use_seq = Convert.ToInt32(reader4["land_use_seq"]),

                });
            }


            reader4.Close();

            ViewBag.tb_land_use = tb_land_use;

            ViewBag.ownerland = ownerland;
            ViewBag.career = career;
            ViewBag.type_fatmer = type_fatmer;

            // id =  1=บุคลลทั่วไป,2=องค์กร // เป็นตารางใน database แยกอยู่แล้ว
            // type = 1=เจ้าของที่ดิน,2=ผู้ต้องการใช้ประโยชน์
            TempData["type_status_regis"] = id;
            TempData["type"] = type;

            TempData["capchar"] = capchar(7);

            return View("Index", ownerland);
        }




        public ActionResult SubmitRegisterfar(Int32[] id_career1, Int32[] id_land_use)
        {

            /* ViewData["firstname"] = o_name;
             ViewData["lastname"] = o_surname;
             ViewData["email"] = o_idcard;*/

            string far_name = Request.Form["far_name"];
            string far_surname = Request.Form["far_surname"];
            string id_card = Request.Form["id_card"];
            string far_age = Request.Form["far_age"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];

            

            string far_tel = Request.Form["far_tel"];
            string far_email = Request.Form["far_email"];
            string far_line = Request.Form["far_line"];
            string far_address = Request.Form["far_address"];
            string far_road = Request.Form["far_road"];
            string id_type_farmer = Request.Form["id_type_farmer"];


            string far_pass = CreatePassword(5);

            var pass = GenerateSHA512String(far_pass);

            //string far_pass = Request.Form["far_pass"];

            //var pass = GenerateSHA512String(far_pass);

            string f_wland = Request.Form["f_wland"];
            string wsize_land = Request.Form["wsize_land"];
            //string money_capital = Request.Form["money_capital"];


            string land_rai = Request.Form["land_rai"];
            string land_nga = Request.Form["land_nga"];
            string land_va = Request.Form["land_va"];

            string reg_capital = Request.Form["reg_capital"];

            string type_status_regis = Request.Form["type_status_regis"];

            string txtfar_code = Request.Form["txtfar_code"];

            

            //string id_career1 = Request.Form["id_career1"];


            // string Role = "Role,Rolename,Rolepwd";
            string[] DateArray = far_age.Split('-');
            string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];

           

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();


            MySqlCommand command = connection.CreateCommand();
           
            if (Convert.ToInt32(type_status_regis) == 1)
            {
                command.CommandText = "select count(*)+1 as ccl from tb_regfarmer where type_status_regis='1'";
            }

            if (Convert.ToInt32(type_status_regis) == 2)
            {
                command.CommandText = "select count(*)+1 as ccl from tb_regfarmer where type_status_regis='2'";
            }

                MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                ViewData["ccl"] = reader2["ccl"].ToString();
                var ccl = txtfar_code + "00" + reader2["ccl"].ToString();
                reader2.Close();
           
            

            command.CommandText = "insert into tb_regfarmer " +
                "(far_name," +
                "far_surname," +
                "id_card," +
                "far_age," +
                "PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "far_tel," +
                "far_email," +
                "far_line," +
                "far_address," +
                "far_road," +
                "id_type_farmer," +
                "far_pass,"+
                "type_status_regis," +
                "far_code" +

                //"f_wland," +
                //"wsize_land," +
                //"money_capital" +

                ")" +
                "VALUES " +
                "('" + far_name + "'," +
                "'" + far_surname + "'," +
                "'" + id_card + "'," +
                "'" + datebirth + "'," +
                "'" + PROVINCE_ID + "'," +
                "'" + AMPHUR_ID + "'," +
                "'" + DISTRICT_ID + "'," +
                "'" + far_tel + "'," +
                "'" + far_email + "'," +
                 "'" + far_line + "'," +
                "'" + far_address + "'," +
                "'" + far_road + "'," +
                "'" + id_type_farmer + "'," +
                "'" + pass + "'," +
                "'" + type_status_regis + "'," +
                "'" + ccl + "'" +

                //"'" + f_wland + "'," +
                //"'" + wsize_land + "'," +
                // "'" + money_capital + "'" +
                ")";
            }
            int i = command.ExecuteNonQuery();
            long far_id = command.LastInsertedId;

            var changeid = CreatePassword(3) + far_id + CreatePassword(50);
            connection.Close();


            //return View("About");

            if (i >= 1)
            {



                foreach (Int32 data_career in id_career1)
                {
                    connection.Open();


                    MySqlCommand command2 = connection.CreateCommand();


                    command2.CommandText = "insert into r_career " +
                        "(id_career," +
                        "far_id" +

                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + data_career + "'," +
                        "'" + far_id + "'" +


                        ")";
                    int i2 = command2.ExecuteNonQuery();


                    connection.Close();

                }


                string fPROVINCE_ID = Request.Form["fPROVINCE_ID"];
                string fAMPHUR_ID = Request.Form["famp_id"];
                string fDISTRICT_ID = Request.Form["fdistr_id"];

                connection.Open();
                MySqlCommand command3 = connection.CreateCommand();


               int cf_wland = Convert.ToInt32(Request.Form["f_wland"]);
                if (cf_wland > 0) { 
                command3.CommandText = "insert into land_famer " +
                    "(PROVINCE_ID," +
                    "AMPHUR_ID," +
                    "DISTRICT_ID," +
                    "far_id," +
                    "land_rai," +
                    "land_nga," +
                    "land_va," +
                    "type_farmer_regis," +
                    "f_wland," +
                    "reg_capital" +

                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + fPROVINCE_ID + "'," +
                    "'" + fAMPHUR_ID + "'," +
                    "'" + fDISTRICT_ID + "'," +
                    "'" + far_id + "'," +
                    "'" + land_rai + "'," +
                    "'" + land_nga + "'," +
                    "'" + land_va + "'," +
                    "'1'," +
                     "'" + f_wland + "'," +
                     "'" + reg_capital + "'" +


                    ")";
                int i3 = command3.ExecuteNonQuery();
                long id_land_famer = command3.LastInsertedId;
                
                connection.Close();





                int idd = id_land_use.Count() - 1;


                //fdetail_user_land = Request.Form.GetValues("fdetail_user_land");
                string fdetail_user_land = Request.Form["fdetail_user_land"];
                string[] ids2 = Request.Form.GetValues("id_land_use");

                connection.Open();
                for (i = 0; i <= idd; i++)
                {
                   

                    MySqlCommand command33 = connection.CreateCommand();
                   
                    command33.CommandText = "insert into r_farmer_land_use " +
                    "(id_land_use," +
                    "far_id," +
                     "fdetail_user_land," +
                     "id_land_famer" +

                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + id_land_use[i] + "'," +
                    "'" + far_id + "'," +
                    "'" + fdetail_user_land + "'," +
                    "'" + id_land_famer + "'" +


                    ")";

                    int i33 = command33.ExecuteNonQuery();
                    

                }
                connection.Close();
                }
                string Url = "wwww.google.com";


                TempData["id_card"] = id_card;
                TempData["far_pass"] = far_pass;
                TempData["Url"] = Url;

                TempData["data_owner"] = far_name + "," + far_surname+","+ far_pass+","+ far_email;

                //ViewBag.Message = "owner";
                //ViewBag.check = "true";

                TempData["farmer"] = "farmer";
                TempData["check"] = "true";
                TempData["far_id"] = changeid;



                return RedirectToAction("confrim_regis_farmer", "Home");

       
            }
            else
            {

                // ViewBag.Message = "owner";
                //ViewBag.check = "false";
                TempData["farmer"] = "farmer";
                TempData["check"] = "false";

                return RedirectToAction("confrim_regis_farmer", "Home");
            }

        }


        public ActionResult UpdateRegisterfar(Int32[] id_career1, string[] fdetail_user_land)
        {

            /* ViewData["firstname"] = o_name;
             ViewData["lastname"] = o_surname;
             ViewData["email"] = o_idcard;*/

            string far_name = Request.Form["far_name"];
            string far_surname = Request.Form["far_surname"];
            string id_card = Request.Form["id_card"];
            string far_age = Request.Form["far_age2"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];



            string far_tel = Request.Form["far_tel"];
            string far_email = Request.Form["far_email"];
            string far_line = Request.Form["far_line"];
            string far_address = Request.Form["far_address"];
            string far_road = Request.Form["far_road"];
            string id_type_farmer = Request.Form["id_type_farmer"];

            string type_status_regis = Request.Form["type_status_regis"];

            //string f_wland = Request.Form["f_wland"];
            //string wsize_land = Request.Form["wsize_land"];
            //string money_capital = Request.Form["money_capital"];



            string[] DateArray = far_age.Split('-');
            string datebirth = DateArray[2] + "-" + DateArray[1] + "-" + DateArray[0];

            string far_id = Request.Form["far_id"];

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (PROVINCE_ID != "0")
            {
                command.CommandText = "update tb_regfarmer set " +
                    "far_name='" + far_name + "'," +
                    "far_surname='" + far_surname + "'," +
                    "far_age='" + datebirth + "'," +
                    "PROVINCE_ID='" + PROVINCE_ID + "'," +
                    "AMPHUR_ID='" + AMPHUR_ID + "'," +
                    "DISTRICT_ID='" + DISTRICT_ID + "'," +
                    "far_tel='" + far_tel + "'," +
                    "far_email='" + far_email + "'," +
                    "far_line='" + far_line + "'," +
                    "far_address='" + far_address + "'," +
                    "far_road='" + far_road + "'," +
                    "id_type_farmer='" + id_type_farmer + "'" +
                    //"type_status_regis='" + type_status_regis + "'" +
                    //"f_wland='" + f_wland + "'," +
                    //"wsize_land='" + wsize_land + "'," +
                    //"money_capital='" + money_capital + "'" +
                    "where far_id='" + far_id + "'";
            }
            else
            {
                command.CommandText = "update tb_regfarmer set " +
                    "far_name='" + far_name + "'," +
                    "far_surname='" + far_surname + "'," +
                    "far_age='" + datebirth + "'," +
                    "far_tel='" + far_tel + "'," +
                    "far_email='" + far_email + "'," +
                    "far_line='" + far_line + "'," +
                    "far_address='" + far_address + "'," +
                    "far_road='" + far_road + "'," +
                    "id_type_farmer='" + id_type_farmer + "'" +
                    //"type_status_regis='" + type_status_regis + "'" +
                    //"f_wland='" + f_wland + "'," +
                    //"wsize_land='" + wsize_land + "'," +
                    //"money_capital='" + money_capital + "'" +
                    "where far_id='" + far_id + "'";

            }
            int i = command.ExecuteNonQuery();
            

            
            connection.Close();

            connection.Open();
            command.CommandText = "delete from r_career where far_id='" + far_id + "'";
            int deli = command.ExecuteNonQuery();
            connection.Close();

            //if (deli>1) { 
            foreach (Int32 data_career in id_career1)
                {
                    connection.Open();


                    MySqlCommand command2 = connection.CreateCommand();


                    command2.CommandText = "insert into r_career " +
                        "(id_career," +
                        "far_id" +

                        ")" +
                        "VALUES " +
                        "(" +

                        "'" + data_career + "'," +
                        "'" + far_id + "'" +


                        ")";
                    int i2 = command2.ExecuteNonQuery();


                    connection.Close();

                }

           // }


            



            Session["alert"] = "Yes";


            if (Session["far_id"] != null)
            {
                return RedirectToAction("data_farmer", "Home");
            }
            else
            {
                return RedirectToAction("data_farmer/"+ far_id, "Home");

            }
                


           

        }


      

        public ActionResult confrim_regis_farmer()
        {

            ViewBag.Message = "confrim_regis_farmer";

            return View("index");
        }

        public ActionResult landfarmer()
        {
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterModel> type_land = new List<RegisterModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }


            reader.Close();

          /*  MySqlCommand command2 = connection.CreateCommand();
            command2.CommandText = "select * from type_land";
            MySqlDataReader reader2 = command2.ExecuteReader();

            while (reader2.Read())
            {

                type_land.Add(new RegisterModel
                {
                    type_land_id = Convert.ToInt32(reader2["type_land_id"]),
                    name_type = reader2["name_type"].ToString()

                });
            }
            reader2.Close();*/

            MySqlCommand command5 = connection.CreateCommand();
            command5.CommandText = "select * from tb_land_use where id_land_use in ('1','2','3','6')";
            MySqlDataReader reader5 = command5.ExecuteReader();

            while (reader5.Read())
            {

                tb_land_use.Add(new RegisterModel
                {
                    id_land_use = Convert.ToInt32(reader5["id_land_use"]),
                    land_use_name = reader5["land_use_name"].ToString(),
                    land_use_seq = Convert.ToInt32(reader5["land_use_seq"]),

                });
            }


            reader5.Close();

            ViewBag.ownerland = ownerland;
            ViewBag.tb_land_use = tb_land_use;
            // ViewBag.type_land = type_land;
            ViewBag.Message = "landfarmer";

            return View("index");
        }


        public ActionResult update_farmerland(Int32[] id_land_use)
        {

            

            string type_land_id = Request.Form["type_land_id"];
            string land_rai = Request.Form["land_rai"];
            string land_nga = Request.Form["land_nga"];
            string land_va = Request.Form["land_va"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["famp_id"];
            string DISTRICT_ID = Request.Form["fdistr_id"];

            string f_wland = Request.Form["f_wland"];



            string far_id = Request.Form["far_id"];

            string reg_capital = Request.Form["reg_capital"];

            string reg_member = Request.Form["reg_member"];
            string reg_income = Request.Form["reg_income"];



            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "insert into land_famer " +
                "(PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "far_id," +
                "land_rai," +
                "land_nga," +
                "land_va," +
                "type_land_id," +
                "type_farmer_regis," +
                 "f_wland," +
                 "reg_capital," +
                 "reg_member," +
                 "reg_income" +
                 ")" +
                "VALUES " +
                "('" + PROVINCE_ID + "'," +
                "'" + AMPHUR_ID + "'," +
                "'" + DISTRICT_ID + "'," +
                "'" + far_id + "'," +
                "'" + land_rai + "'," +
                "'" + land_nga + "'," +
                "'" + land_va + "'," +
                "'" + type_land_id + "'," +
                "'"+ Session["type_farmer_regis"] + "'," +
                 "'" + f_wland + "'," +
                 "'" + reg_capital + "'," +
                 "'" + reg_member + "'," +
                 "'" + reg_income + "'" +
                ")";
            int i = command.ExecuteNonQuery();
            var id_land_famer = command.LastInsertedId;


            connection.Close();

           

            connection.Open();
           // command.CommandText = "delete from r_farmer_land_use where far_id='" + far_id + "'";
            //int deli2 = command.ExecuteNonQuery();
            connection.Close();

            // if (deli2 > 1)
            //{
            int idd = id_land_use.Count() - 1;


            
            string[] ids2 = Request.Form.GetValues("id_land_use");
            string fdetail_user_land = Request.Form["fdetail_user_land"];

            connection.Open();
            for (i = 0; i <= idd; i++)
            {


                MySqlCommand command33 = connection.CreateCommand();

                command33.CommandText = "insert into r_farmer_land_use " +
                "(id_land_use," +
                "far_id," +
                  "id_land_famer," +
                  "fdetail_user_land" +

                ")" +
                "VALUES " +
                "(" +

                "'" + id_land_use[i] + "'," +
                "'" + far_id + "'," +
                 "'" + id_land_famer + "'," +
                 "'"+ fdetail_user_land + "'" +
               


                ")";

                int i33 = command33.ExecuteNonQuery();


            }

            connection.Close();
            // }



            Session["alert"] = "Yes";

            return RedirectToAction("landfarmer", "Home");

            //return View("About");

          

        }


        public ActionResult listland_farmer()
        {

            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
           // string[] DateArray = Url.RequestContext.RouteData.Values["id"].ToString().Split('-');
            //string far_id = DateArray[0];
            ///string type_farmer_regis = DateArray[1];
            var far_id = Url.RequestContext.RouteData.Values["id"];
            // Response.Write("<script>alert('" + show_province + "')</script>");

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterFarnerModel> land = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> landuser = new List<RegisterFarnerModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select * from land_famer as ll left join province as p on(p.PROVINCE_ID=ll.PROVINCE_ID) left join amphur as a on(a.AMPHUR_ID=ll.AMPHUR_ID) left join district as d on(d.DISTRICT_ID=ll.DISTRICT_ID) where ll.far_id='" + far_id + "' and type_farmer_regis ='" + Session["type_farmer_regis"] + "'";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                land.Add(new RegisterFarnerModel
                {
                    id_land_famer = Convert.ToInt32(reader["id_land_famer"]),
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                    DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    far_id = reader["far_id"].ToString(),
                    land_rai = reader["land_rai"].ToString(),
                    land_nga = reader["land_nga"].ToString(),
                    land_va = reader["land_va"].ToString(),
                    type_land_id = Convert.ToInt32(reader["type_land_id"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    f_wland = Convert.ToInt32(reader["f_wland"]),
                    reg_member = reader["reg_member"].ToString(),
                    reg_income = reader["reg_income"].ToString(),
                    reg_capital = reader["reg_capital"].ToString(),

                });
            }



            reader.Close();


           
            

            //MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select * from r_farmer_land_use as us inner join tb_land_use as tu on(tu.id_land_use=us.id_land_use)  where us.far_id='" + far_id + "'";
            MySqlDataReader reader2 = command.ExecuteReader();
            while (reader2.Read())
            {

                landuser.Add(new RegisterFarnerModel
                {
                    id_land_famer = Convert.ToInt32(reader2["id_land_famer"]),
                    id_land_use = Convert.ToInt32(reader2["id_land_use"]),
                    land_use_name = reader2["land_use_name"].ToString(),



                });
            }



            reader2.Close();

            connection.Close();

            ViewBag.landuser = landuser;


            return View("listland_farmer", land);


        }


        



        public ActionResult delete_farmerland(int id_land_famer)
        {

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "delete from land_famer where id_land_famer='" + id_land_famer + "'";
            int i = command.ExecuteNonQuery();

            command.CommandText = "delete from r_farmer_land_use where id_land_famer='" + id_land_famer + "'";
            int deli2 = command.ExecuteNonQuery();

            connection.Close();

            Session["alert"] = "Del";
            return RedirectToAction("landfarmer", "Home");
        }


        public ActionResult showsearch()
        {

            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
            var id = Url.RequestContext.RouteData.Values["id"];
            // Response.Write("<script>alert('" + show_province + "')</script>");

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


           // command.CommandText = "select * from owner_land as ll left join province as p on(p.PROVINCE_ID=ll.PROVINCE_ID) left join amphur as a on(a.AMPHUR_ID=ll.AMPHUR_ID) left join district as d on(d.DISTRICT_ID=ll.DISTRICT_ID)" +
           //     "where ll.PROVINCE_ID ='" + id + "' or ll.AMPHUR_ID ='" + id + "' or ll.DISTRICT_ID='" + id + "' and status_land='2'";

            command.CommandText = "select " +
                "count(ow.o_land_id) as ccl,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.period_per," +
                "ow.period,ow.type_land_id,ow.dsave,ow.land_rai,ow.land_nga,ow.land_va,dd.img_name," +
                "ow.codition,ow.m_rent,ow.place_land,ow.status_land," +
                "ow.latitude_land,ow.longitude_land,ow.o_wland from owner_land as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +


                "where (ow.PROVINCE_ID='" + id + "' or ow.AMPHUR_ID='"+id+ "' or ow.DISTRICT_ID='"+id+ "') and status_land ='2'" +
                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave";

            MySqlDataReader reader = command.ExecuteReader();

            
                    while (reader.Read())
                    {

                        land.Add(new RegisterModel
                        {
                            o_land_id = Convert.ToInt32(reader["o_land_id"]),
                            //PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                            //AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                            //DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                            //type_land_id = reader["type_land_id"].ToString(),
                            land_rai = reader["land_rai"].ToString(),
                            land_nga = reader["land_nga"].ToString(),
                            land_va = reader["land_va"].ToString(),
                            type_land_id = Convert.ToInt32(reader["type_land_id"]),
                            PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                            AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                            DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                            m_rent = reader["m_rent"].ToString(),
                            period_per = reader["period_per"].ToString(),
                            period = reader["period"].ToString(),
                            codition = reader["codition"].ToString(),
                            latitude_land = reader["latitude_land"].ToString(),
                            longitude_land = reader["longitude_land"].ToString(),
                            img_name = reader["img_name"].ToString(),
                            dsave = reader["dsave"].ToString(),
                            place_land = reader["place_land"].ToString(),
                            ccl = reader["ccl"].ToString(),
                            o_wland = Convert.ToInt32(reader["o_wland"]),

                        });
                    }

            reader.Close();


            command.CommandText = "select count(*) as ccl from owner_land as ow " +
                "where (ow.PROVINCE_ID='" + id + "' or ow.AMPHUR_ID='" + id + "' or ow.DISTRICT_ID='" + id + "') and status_land ='2'";

            MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                ViewData["ccl"] = reader2["ccl"].ToString();
                reader2.Close();
            }




            return View("search_list", land);


            /* if (reader.Read())
             {
                 reader.Close();
                 return View("search_list", land);
             }
             else
             {
                 return Content("00");


             }*/





        }

        public ActionResult showsearchall()
        {

            //ViewBag.Message = "Your application description page.";

            //string show_province = Request.QueryString["AMPHUR_ID"];
            var id = Url.RequestContext.RouteData.Values["id"];
            // Response.Write("<script>alert('" + show_province + "')</script>");

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            // command.CommandText = "select * from owner_land as ll left join province as p on(p.PROVINCE_ID=ll.PROVINCE_ID) left join amphur as a on(a.AMPHUR_ID=ll.AMPHUR_ID) left join district as d on(d.DISTRICT_ID=ll.DISTRICT_ID)" +
            //     "where ll.PROVINCE_ID ='" + id + "' or ll.AMPHUR_ID ='" + id + "' or ll.DISTRICT_ID='" + id + "' and status_land='2'";

            command.CommandText = "select " +
                "count(ow.o_land_id) as ccl,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.period_per," +
                "ow.period,ow.type_land_id,ow.dsave,ow.land_rai,ow.land_nga,ow.land_va,dd.img_name," +
                "ow.codition,ow.m_rent,ow.place_land,ow.status_land," +
                "ow.latitude_land,ow.longitude_land,ow.o_wland from owner_land as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +


                "where status_land ='2'" +
                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave";

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    o_land_id = Convert.ToInt32(reader["o_land_id"]),
                    //PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    //AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                    //DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    //type_land_id = reader["type_land_id"].ToString(),
                    land_rai = reader["land_rai"].ToString(),
                    land_nga = reader["land_nga"].ToString(),
                    land_va = reader["land_va"].ToString(),
                    type_land_id = Convert.ToInt32(reader["type_land_id"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    m_rent = reader["m_rent"].ToString(),
                    period_per = reader["period_per"].ToString(),
                    period = reader["period"].ToString(),
                    codition = reader["codition"].ToString(),
                    latitude_land = reader["latitude_land"].ToString(),
                    longitude_land = reader["longitude_land"].ToString(),
                    img_name = reader["img_name"].ToString(),
                    dsave = reader["dsave"].ToString(),
                    place_land = reader["place_land"].ToString(),
                    ccl = reader["ccl"].ToString(),
                    o_wland = Convert.ToInt32(reader["o_wland"]),
                    
                });
            }

            reader.Close();


            command.CommandText = "select count(*) as ccl from owner_land as ow " +
                "where status_land ='2'";

            MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                ViewData["ccl"] = reader2["ccl"].ToString();
                reader2.Close();
            }

            return View("search_list", land);


            /* if (reader.Read())
             {
                 reader.Close();
                 return View("search_list", land);
             }
             else
             {
                 return Content("00");


             }*/





        }




        public ActionResult showsearch_inarea()
        {

         
            var id = Url.RequestContext.RouteData.Values["id"];

           
            

           
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


           

            command.CommandText = "select " +
                "count(ow.o_land_id) as ccl,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.period_per," +
                "ow.period,ow.type_land_id,ow.dsave,ow.land_rai,ow.land_nga,ow.land_va,dd.img_name," +
                "ow.codition,ow.m_rent,ow.place_land,ow.status_land," +
                "ow.latitude_land,ow.longitude_land,ow.o_wland from owner_land as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join img_land as dd on(dd.o_land_id=ow.o_land_id)" +
                "LEFT JOIN land_famer as lf ON(ow.DISTRICT_ID=lf.DISTRICT_ID)" +


                "where (lf.far_id='" + id + "' and lf.type_farmer_regis='" + Session["type_farmer_regis"] + "') and ow.status_land ='2'" +
                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave";
            

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    o_land_id = Convert.ToInt32(reader["o_land_id"]),
                    //PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    //AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                    //DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    //type_land_id = reader["type_land_id"].ToString(),
                    land_rai = reader["land_rai"].ToString(),
                    land_nga = reader["land_nga"].ToString(),
                    land_va = reader["land_va"].ToString(),
                    type_land_id = Convert.ToInt32(reader["type_land_id"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    m_rent = reader["m_rent"].ToString(),
                    period_per = reader["period_per"].ToString(),
                    period = reader["period"].ToString(),
                    codition = reader["codition"].ToString(),
                    latitude_land = reader["latitude_land"].ToString(),
                    longitude_land = reader["longitude_land"].ToString(),
                    img_name = reader["img_name"].ToString(),
                    dsave = reader["dsave"].ToString(),
                    place_land = reader["place_land"].ToString(),
                    ccl = reader["ccl"].ToString(),
                    o_wland = Convert.ToInt32(reader["o_wland"]),


                });
            }

            reader.Close();


            command.CommandText = "select count(*) as ccl from owner_land as ow LEFT JOIN land_famer as lf ON(ow.DISTRICT_ID=lf.DISTRICT_ID) " +
                "where (lf.far_id='" + id + "' and lf.type_farmer_regis='" + Session["type_farmer_regis"] + "') and ow.status_land ='2'";

            MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                ViewData["ccl"] = reader2["ccl"].ToString();
                reader2.Close();
            }


            ViewData["textarea"] = "(ที่ดินในพื้นที่ที่ท่านต้องการ)";
            ViewData["idlocaltion"] = -100;
            return View("search_list", land);


           

        }




        public ActionResult showland_search(int id)
        {
            ViewBag.Message = "showland_search";
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterModel> type_land = new List<RegisterModel>();
            List<RegisterModel> img_land = new List<RegisterModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();
            List<RegisterModel> tb_water = new List<RegisterModel>();

            connection.Open();

            MySqlCommand command = connection.CreateCommand();

           
            if (Session["far_id"] != null)
            {
                command.CommandText = "select (select count(*) from tb_interested where o_land_id=ow.o_land_id) as cinter,(select count(*) from tb_interested where far_id='" + Session["far_id"] + "' and o_land_id=ow.o_land_id and type_inter=1) as cfar_id," +
             "t.name_type,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land,ow.o_wland,ow.o_road from owner_land as ow " +
             "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
             "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
             "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
             "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
             "where ow.o_land_id='" + id + "'";
            }
            else if(Session["reg_organi_id"] != null)
            {
                  command.CommandText = "select (select count(*) from tb_interested where o_land_id=ow.o_land_id) as cinter,(select count(*) from tb_interested where far_id='" + Session["reg_organi_id"] + "' and o_land_id=ow.o_land_id and type_inter=2) as cfar_id," +
              "t.name_type,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land,ow.o_wland,ow.o_road from owner_land as ow " +
              "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
              "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
              "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
              "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
              "where ow.o_land_id='" + id + "'";
            }
            else
            {
                command.CommandText = "select (select count(*) from tb_interested where o_land_id=ow.o_land_id) as cinter,(select count(*) from tb_interested where far_id='-1' and o_land_id=ow.o_land_id and type_inter=1) as cfar_id," +
              "t.name_type,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land,ow.o_wland,ow.o_road from owner_land as ow " +
              "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
              "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
              "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
              "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
              "where ow.o_land_id='" + id + "'";
            }



            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {



                ViewData["o_land_id"] = Convert.ToInt32(reader["o_land_id"]);
                ViewData["PROVINCE_NAME"] = reader["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader["DISTRICT_NAME"].ToString();
                ViewData["dsave"] = reader["dsave"].ToString();
                ViewData["m_rent"] = reader["m_rent"].ToString();
                ViewData["place_land"] = reader["place_land"].ToString();
                ViewData["type_land_id"] = reader["type_land_id"].ToString();
                ViewData["land_rai"] = reader["land_rai"].ToString();
                ViewData["land_nga"] = reader["land_nga"].ToString();
                ViewData["land_va"] = reader["land_va"].ToString();
                ViewData["period_per"] = reader["period_per"].ToString();
                ViewData["period"] = reader["period"].ToString();
                ViewData["codition"] = reader["codition"].ToString();
                ViewData["latitude_land"] = reader["latitude_land"].ToString();
                ViewData["longitude_land"] = reader["longitude_land"].ToString();
                ViewData["place_land"] = reader["place_land"].ToString();
                ViewData["name_type"] = reader["name_type"].ToString();
                ViewData["cinter"] = reader["cinter"].ToString();
                TempData["cfar_id"] = reader["cfar_id"].ToString();
                ViewData["o_wland"] = Convert.ToInt32(reader["o_wland"]);
                TempData["o_road"] = reader["o_road"].ToString();
                



                reader.Close();

                




                /*
                MySqlCommand command1 = connection.CreateCommand();
                command1.CommandText = "select * from province";
                MySqlDataReader reader1 = command1.ExecuteReader();

                while (reader1.Read())
                {

                    ownerland.Add(new RegisterModel
                    {
                        PROVINCE_ID = Convert.ToInt32(reader1["PROVINCE_ID"]),
                        PROVINCE_NAME = reader1["PROVINCE_NAME"].ToString(),

                    });
                }
                reader1.Close();
                
                MySqlCommand command2 = connection.CreateCommand();
                command2.CommandText = "select * from type_land";
                MySqlDataReader reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {

                    type_land.Add(new RegisterModel
                    {
                        type_land_id = Convert.ToInt32(reader2["type_land_id"]),
                        name_type = reader2["name_type"].ToString()

                    });
                }


                reader2.Close();
                */

                //MySqlCommand command2 = connection.CreateCommand();
                command.CommandText = "select * from img_land where o_land_id = '"+ id  + "'";
                MySqlDataReader reader3 = command.ExecuteReader();

                while (reader3.Read())
                {

                    img_land.Add(new RegisterModel
                    {
                        img_id = Convert.ToInt32(reader3["img_id"]),
                        img_name = reader3["img_name"].ToString()

                    });
                }


                reader3.Close();

                command.CommandText = "select * from r_owner_land_use as rl inner join tb_land_use as tu on (tu.id_land_use=rl.id_land_use)  where o_land_id = '" + id + "'";
                MySqlDataReader reader4 = command.ExecuteReader();

                while (reader4.Read())
                {

                    tb_land_use.Add(new RegisterModel
                    {
                        // img_id = Convert.ToInt32(reader4["land_use_name"]),
                        land_use_name = reader4["land_use_name"].ToString()

                    });
                }


                reader4.Close();


                
                command.CommandText = "select * from r_land_water as rl inner join tb_water as tu on (tu.id_data_water=rl.id_data_water)  where o_land_id = '" + id + "'";
                MySqlDataReader reader5 = command.ExecuteReader();

                while (reader5.Read())
                {

                    tb_water.Add(new RegisterModel
                    {
                        // img_id = Convert.ToInt32(reader4["land_use_name"]),
                        water_name = reader5["water_name"].ToString()

                    });
                }


                reader5.Close();


                List<RegisterModel> land_group = new List<RegisterModel>();
                command.CommandText = "select * from r_land_group as rl inner join land_group as tu on (tu.id_land_group=rl.id_land_group)  where o_land_id = '" + id + "'";
                MySqlDataReader reader6 = command.ExecuteReader();

                while (reader6.Read())
                {

                    land_group.Add(new RegisterModel
                    {
                        // img_id = Convert.ToInt32(reader4["land_use_name"]),
                        land_group_name = reader6["land_group_name"].ToString()

                    });
                }


                reader6.Close();



                List<RegisterModel> condition_use_land = new List<RegisterModel>();
                command.CommandText = "select * from r_condi_land as rl inner join condition_use_land as tu on (tu.id_condi_use_land=rl.id_condi_use_land)  where o_land_id = '" + id + "'";
                MySqlDataReader reader7 = command.ExecuteReader();

                while (reader7.Read())
                {

                    condition_use_land.Add(new RegisterModel
                    {
                        // img_id = Convert.ToInt32(reader4["land_use_name"]),
                        condi_name = reader7["condi_name"].ToString()

                    });
                }


                reader7.Close();

                ViewBag.type_land = type_land;
                ViewBag.ownerland = ownerland;
                ViewBag.img_land = img_land;
                ViewBag.tb_land_use = tb_land_use;
                ViewBag.tb_water = tb_water;
                ViewBag.land_group = land_group;
                ViewBag.condition_use_land = condition_use_land;




                //return RedirectToAction("Index", "Home");
                return View("Index");

            }
            else
            {

                return View("Index");
            }
        }


        public ActionResult map_api(string id)
        {
            //string iid = id;
            //var id = Url.RequestContext.RouteData.Values["id"];
            string[] focus = id.Split('-');
            //string[] focus = Url.RequestContext.RouteData.Values["id"].ToString().Split('-');
            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> polygon = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from tb_polygon_owner where o_land_id = '" + focus[2] + "'";
            MySqlDataReader reader3 = command.ExecuteReader();

            while (reader3.Read())
            {

                polygon.Add(new RegisterModel
                {
                    polygon_land_id = Convert.ToInt32(reader3["polygon_land_id"]),
                    polygon_lata = reader3["polygon_lata"].ToString(),
                    polygon_ing = reader3["polygon_ing"].ToString()

                });
            }


            ViewBag.latitude_land = focus[0];
            ViewBag.longitude_land = focus[1];
            ViewBag.polygon = polygon;
            return View("api_map");
        }
        
        
       public ActionResult interrested()
        {
            string inter_name = Request.Form["inter_name"];
            string inter_phone = Request.Form["inter_phone"];
            string inter_word = Request.Form["inter_word"];
            string o_land_id = Request.Form["o_land_id"];
            string far_id = Request.Form["far_id"];
            string type_inter = Request.Form["type_inter"];

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "insert into tb_interested " +
                "(inter_name," +
                "inter_phone," +
                "inter_word," +
                "o_land_id," +
                "far_id," +
                "type_inter" +
                 ")" +
                "VALUES " +
                "('" + inter_name + "'," +
                "'" + inter_phone + "'," +
                "'" + inter_word + "'," +
                "'" + o_land_id + "'," +
                "'" + far_id + "'," +
                "'" + type_inter + "'" +

                ")";
            int i = command.ExecuteNonQuery();



            connection.Close();

            Session["alert"] = "interrested";
            return RedirectToAction("showland_search", "Home", new { id = o_land_id });

            
        }

        public ActionResult check_phone_inter()
        {

            string[] DateArray = Url.RequestContext.RouteData.Values["id"].ToString().Split('-');
            string inter_phone = DateArray[0];
            string o_land_id = DateArray[1];

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> owner = new List<RegisterModel>();
            
            MySqlCommand command = connection.CreateCommand();
            
            command.CommandText = "select count(*)  from tb_interested where inter_phone='" + inter_phone + "' and o_land_id ='"+o_land_id+"'";
            connection.Open();
            //MySqlDataReader reader = command.ExecuteReader();
            var NumRows = (long)command.ExecuteScalar();
            connection.Close();



            if (NumRows >= 1)
            {

                return Content("1");
            }
            else
            {
                return Content("0");

            }



        }

        public ActionResult vision()
        {
            ViewBag.Message = "vision";
            return View("Index");
        }

        public ActionResult history()
        {
            ViewBag.Message = "history";
            return View("Index");
        }

        public ActionResult network()
        {
            ViewBag.Message = "network";
            return View("Index");
        }

        public ActionResult flow()
        {
            ViewBag.Message = "flow";
            return View("Index");
        }


        public ActionResult mission()
        {
            ViewBag.Message = "mission";
            return View("Index");
        }

        public ActionResult register_organi(Int32 id, Int32 type)
        {



            ViewBag.Message = "register_organi";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterFarnerModel> career = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> type_fatmer = new List<RegisterFarnerModel>();
            List<RegisterModel> organi = new List<RegisterModel>();
            List<RegisterModel> tb_land_use = new List<RegisterModel>();

            
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }


            reader.Close();



           
            command.CommandText = "select * from type_organi";
            MySqlDataReader reader4 = command.ExecuteReader();

            while (reader4.Read())
            {

                organi.Add(new RegisterModel
                {
                    type_organi_id = Convert.ToInt32(reader4["type_organi_id"]),
                    name_organi = reader4["name_organi"].ToString(),


                });
            }


            reader4.Close();

            MySqlCommand command5 = connection.CreateCommand();
            command5.CommandText = "select * from tb_land_use where id_land_use in ('1','2','3','6')";
            MySqlDataReader reader5 = command5.ExecuteReader();

            while (reader5.Read())
            {

                tb_land_use.Add(new RegisterModel
                {
                    id_land_use = Convert.ToInt32(reader5["id_land_use"]),
                    land_use_name = reader5["land_use_name"].ToString(),
                    land_use_seq = Convert.ToInt32(reader5["land_use_seq"]),

                });
            }


            reader5.Close();

            ViewBag.ownerland = ownerland;
            ViewBag.organi = organi;
            ViewBag.tb_land_use = tb_land_use;
            TempData["type_status_regis"] = id;

            TempData["type"] = type;


            TempData["capchar"] = capchar(7);

            return View("Index", ownerland);


           
        }



        public ActionResult SubmitRegisterorgani(Int32[] id_land_use)
        {

           
            string type_organi_id = Request.Form["type_organi_id"];
            string reg_name = Request.Form["reg_name"];
            string reg_numberorgani = Request.Form["reg_numberorgani"];

            string reg_regis_name = Request.Form["reg_regis_name"];
            string reg_posi_name = Request.Form["reg_posi_name"];
            string reg_regis_tel = Request.Form["reg_regis_tel"];
            string reg_email = Request.Form["reg_email"];

            string reg_chief = Request.Form["reg_chief"];
            string reg_ctel = Request.Form["reg_ctel"];

            string reg_vchief = Request.Form["reg_vchief"];
            string reg_vctel = Request.Form["reg_vctel"];
            string reg_num = Request.Form["reg_num"];

            string reg_addr = Request.Form["reg_addr"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];

            string detail_opara = Request.Form["detail_opara"];
            string activity_curent = Request.Form["activity_curent"];
            string reg_results = Request.Form["reg_results"];
            string reg_vision = Request.Form["reg_vision"];
            string reg_plant = Request.Form["reg_plant"];
            string reg_rai = Request.Form["reg_rai"];
            string reg_nga = Request.Form["reg_nga"];
            string reg_wa = Request.Form["reg_wa"];
            string reg_member = Request.Form["reg_member"];
            string reg_income = Request.Form["reg_income"];
            string reg_capital = Request.Form["reg_capital"];
            string reg_accept = Request.Form["reg_accept"];
            string pass = CreatePassword(5);


            string f_wland = Request.Form["f_wland"];
            string type_status_regis_og = Request.Form["type_status_regis_og"];

            string txtoga_code = Request.Form["txtoga_code"];

            var reg_password = GenerateSHA512String(pass);


            //var reg_password = GenerateSHA512String(far_pass);


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (Convert.ToInt32(type_status_regis_og) == 1)
            {
                command.CommandText = "select count(*)+1 as ccl from regis_organi where type_status_regis_og='1'";
            }

            if (Convert.ToInt32(type_status_regis_og) == 2)
            {
                command.CommandText = "select count(*)+1 as ccl from regis_organi where type_status_regis_og='2'";
            }

            MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                ViewData["ccl"] = reader2["ccl"].ToString();
                var ccl = txtoga_code + "00" + reader2["ccl"].ToString();
                reader2.Close();

                command.CommandText = "insert into regis_organi " +
                "(type_organi_id," +
                "reg_name," +
                "reg_numberorgani," +
                "reg_regis_name," +
                "reg_posi_name," +
                "reg_regis_tel," +
                "reg_email," +
                "reg_chief," +
                "reg_ctel," +
                "reg_vchief," +
                "reg_vctel," +
                "reg_num," +
                "reg_addr," +
                "PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "detail_opara," +
                "activity_curent," +
                "reg_results," +
                "reg_vision," +
                //"reg_plant," +
                //"reg_rai," +
                // "reg_nga," +
                // "reg_wa," +
                //"reg_member," +
                // "reg_income," +
                // "reg_capital," +
                "reg_accept," +
                "reg_password," +
                "type_status_regis_og," +
                "oga_code" +

                ")" +
                "VALUES " +
                "('" + type_organi_id + "'," +
                "'" + reg_name + "'," +
                "'" + reg_numberorgani + "'," +
                "'" + reg_regis_name + "'," +
                "'" + reg_posi_name + "'," +
                "'" + reg_regis_tel + "'," +
                "'" + reg_email + "'," +
                "'" + reg_chief + "'," +
                "'" + reg_ctel + "'," +
                "'" + reg_vchief + "'," +
                "'" + reg_vctel + "'," +
                "'" + reg_num + "'," +
                "'" + reg_addr + "'," +
                "'" + PROVINCE_ID + "'," +
                "'" + AMPHUR_ID + "'," +
                "'" + DISTRICT_ID + "'," +
                "'" + detail_opara + "'," +
                "'" + activity_curent + "'," +
                "'" + reg_results + "'," +
                "'" + reg_vision + "'," +
                // "'" + reg_plant + "'," +
                // "'" + reg_rai + "'," +
                // "'" + reg_nga + "'," +
                // "'" + reg_wa + "'," +
                // "'" + reg_member + "'," +
                // "'" + reg_income + "'," +
                // "'" + reg_capital + "'," +
                "'" + reg_accept + "'," +
                 "'" + reg_password + "'," +
                 "'" + type_status_regis_og + "'," +
                 "'" + ccl + "'" +
                ")";
            }
            int i = command.ExecuteNonQuery();
            var reg_organi_id = command.LastInsertedId;

            var changeid = CreatePassword(3) + reg_organi_id + CreatePassword(50);
            

            connection.Close();


            //return View("About");

            if (i >= 1)
            {

                string Url = "wwww.google.com";

                


                TempData["data_organi"] = reg_name + "," + pass + "," + reg_email;
                TempData["Url"] = Url;
                //TempData["reg_organi_id"] = reg_organi_id;
                TempData["reg_organi_id"] = changeid;
                //ViewBag.Message = "owner";
                //ViewBag.check = "true";

                TempData["organi"] = "organi";
                TempData["check"] = "true";
                //TempData["id_owner"] = changeid;



                

                string fPROVINCE_ID = Request.Form["fPROVINCE_ID"];
                string fAMPHUR_ID = Request.Form["famp_id"];
                string fDISTRICT_ID = Request.Form["fdistr_id"];
                string land_rai = Request.Form["land_rai"];
                string land_nga = Request.Form["land_nga"];
                string land_va = Request.Form["land_va"];

                int cf_wland = Convert.ToInt32(Request.Form["f_wland"]);

                if (cf_wland>0) { 
                connection.Open();
                MySqlCommand command3 = connection.CreateCommand();

                
                command3.CommandText = "insert into land_famer " +
                    "(PROVINCE_ID," +
                    "AMPHUR_ID," +
                    "DISTRICT_ID," +
                    "far_id," +
                    "land_rai," +
                    "land_nga," +
                    "land_va," +
                    "type_farmer_regis," +
                    "f_wland," +
                    "reg_member," +
                    "reg_income," +
                    "reg_capital" +
                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + fPROVINCE_ID + "'," +
                    "'" + fAMPHUR_ID + "'," +
                    "'" + fDISTRICT_ID + "'," +
                    "'" + reg_organi_id + "'," +
                    "'" + land_rai + "'," +
                    "'" + land_nga + "'," +
                    "'" + land_va + "'," +
                    "'2'," +
                    "'" + f_wland + "'," +
                    "'" + reg_member + "'," +
                    "'" + reg_income + "'," +
                    "'" + reg_capital + "'" +
                    ")";

                int i3 = command3.ExecuteNonQuery();
                long id_land_famer = command3.LastInsertedId;

                connection.Close();

                
                int idd = id_land_use.Count() - 1;


                //fdetail_user_land = Request.Form.GetValues("fdetail_user_land");
                string fdetail_user_land = Request.Form["fdetail_user_land"];
                string[] ids2 = Request.Form.GetValues("id_land_use");

                connection.Open();
                for (i = 0; i <= idd; i++)
                {


                    MySqlCommand command33 = connection.CreateCommand();

                    command33.CommandText = "insert into r_farmer_land_use " +
                    "(id_land_use," +
                    "far_id," +
                     "id_land_famer," +
                     "fdetail_user_land" +

                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + id_land_use[i] + "'," +
                    "'" + reg_organi_id + "'," +
                    "'" + id_land_famer + "'," +
                    "'"+ fdetail_user_land + "'" +


                    ")";

                    int i33 = command33.ExecuteNonQuery();


                }
                connection.Close();
                }




                return RedirectToAction("confrim_regis_organi", "Home");

                //return Redirect("http://intranet.labai.or.th:8080/apimail/getmail.php?data=" + data);
                //return View("Index", data);
                //Response.Redirect("http://intranet.labai.or.th:8080/apimail/getmail.php?data="+data);
                //return Redirect("http://www.google.com");
            }
            else
            {

                // ViewBag.Message = "owner";
                //ViewBag.check = "false";
                TempData["organi"] = "organi";
                TempData["check"] = "false";

                return RedirectToAction("confrim_regis_organi", "Home");
            }

        }

        public ActionResult confrim_regis_organi()
        {

            ViewBag.Message = "confrim_regis_organi";

            return View("index");
        }

        public ActionResult conforgani(string id)
        {



            string reg_organi_id = id.Substring(3, id.Length - 53);
            ViewBag.textid = reg_organi_id;

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update regis_organi set " +
                     "reg_status ='2'" +
                     " where reg_organi_id = '" + reg_organi_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                ViewBag.Message = "login";
                return View("index");
            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }



        }



        public ActionResult organi_update()
        {



            ViewBag.Message = "organi_update";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            List<RegisterFarnerModel> oraganidata = new List<RegisterFarnerModel>();
            List<RegisterFarnerModel> type_fatmer = new List<RegisterFarnerModel>();
            List<RegisterModel> organi = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }


            reader.Close();




            command.CommandText = "select * from type_organi";
            MySqlDataReader reader4 = command.ExecuteReader();

            while (reader4.Read())
            {

                organi.Add(new RegisterModel
                {
                    type_organi_id = Convert.ToInt32(reader4["type_organi_id"]),
                    name_organi = reader4["name_organi"].ToString(),


                });
            }
            reader4.Close();

            

            if (Session["reg_organi_id"] != null)
            {
                var keyfar_id = Session["far_id"];
                command.CommandText = "select * from regis_organi as ow " +
                 "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                 "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                 "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                 "where ow.reg_organi_id = '" + Session["reg_organi_id"] + "'";
            }
            else
            {
                var keyreg_organi_id = Url.RequestContext.RouteData.Values["id"];
                command.CommandText = "select * from regis_organi as ow " +
                 "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                 "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                 "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                 "where ow.reg_organi_id = '" + keyreg_organi_id + "'";
            }



            MySqlDataReader reader5 = command.ExecuteReader();

            
            if (reader5.Read())
            {
                ViewData["reg_organi_id"] = Convert.ToInt32(reader5["reg_organi_id"]);
                ViewData["type_organi_id"] = Convert.ToInt32(reader5["type_organi_id"]);
                ViewData["reg_name"] = reader5["reg_name"].ToString();
                ViewData["reg_numberorgani"] = reader5["reg_numberorgani"].ToString();
                ViewData["reg_regis_name"] = reader5["reg_regis_name"].ToString();
                ViewData["reg_posi_name"] = reader5["reg_posi_name"].ToString();
                ViewData["reg_regis_tel"] = reader5["reg_regis_tel"].ToString();
                ViewData["reg_email"] = reader5["reg_email"].ToString();
                ViewData["reg_chief"] = reader5["reg_chief"].ToString();
                ViewData["reg_ctel"] = reader5["reg_ctel"].ToString();
                ViewData["reg_vchief"] = reader5["reg_vchief"].ToString();
                ViewData["reg_vctel"] = reader5["reg_vctel"].ToString();
                ViewData["reg_num"] = reader5["reg_num"].ToString();
                ViewData["reg_addr"] = reader5["reg_addr"].ToString();
                ViewData["PROVINCE_ID"] = reader5["PROVINCE_ID"].ToString();
                ViewData["AMPHUR_ID"] = reader5["AMPHUR_ID"].ToString();
                ViewData["DISTRICT_ID"] = reader5["DISTRICT_ID"].ToString();
                ViewData["detail_opara"] = reader5["detail_opara"].ToString();
                ViewData["activity_curent"] = reader5["activity_curent"].ToString();
                ViewData["reg_results"] = reader5["reg_results"].ToString();
                ViewData["reg_vision"] = reader5["reg_vision"].ToString();
                ViewData["reg_plant"] = reader5["reg_plant"].ToString();
                ViewData["reg_rai"] = reader5["reg_rai"].ToString();
                ViewData["reg_nga"] = reader5["reg_nga"].ToString();
                ViewData["reg_wa"] = reader5["reg_wa"].ToString();
                ViewData["reg_member"] = reader5["reg_member"].ToString();
                ViewData["reg_income"] = reader5["reg_income"].ToString();
                ViewData["reg_capital"] = reader5["reg_capital"].ToString();
                ViewData["reg_accept"] = reader5["reg_accept"].ToString();
                ViewData["reg_status"] = reader5["reg_status"].ToString();

                ViewData["PROVINCE_NAME"] = reader5["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader5["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader5["DISTRICT_NAME"].ToString();

                ViewData["type_status_regis_og"] = reader5["type_status_regis_og"].ToString();
                
                reader5.Close();
            }

            ViewBag.ownerland = ownerland;
            ViewBag.organi = organi;

            TempData["capchar"] = capchar(7);

            return View("Index", ownerland);



        }

        public ActionResult Updateorgani()
        {


            string type_organi_id = Request.Form["type_organi_id"];
            string reg_name = Request.Form["reg_name"];
            string reg_numberorgani = Request.Form["reg_numberorgani"];

            string reg_regis_name = Request.Form["reg_regis_name"];
            string reg_posi_name = Request.Form["reg_posi_name"];
            string reg_regis_tel = Request.Form["reg_regis_tel"];
            string reg_email = Request.Form["reg_email"];

            //string reg_chief = Request.Form["reg_chief"];
            //string reg_ctel = Request.Form["reg_ctel"];

            //string reg_vchief = Request.Form["reg_vchief"];
           // string reg_vctel = Request.Form["reg_vctel"];
            string reg_num = Request.Form["reg_num"];

            string reg_addr = Request.Form["reg_addr"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];

            string detail_opara = Request.Form["detail_opara"];
            string activity_curent = Request.Form["activity_curent"];
            string reg_results = Request.Form["reg_results"];
            string reg_vision = Request.Form["reg_vision"];
            string reg_plant = Request.Form["reg_plant"];
            string reg_rai = Request.Form["reg_rai"];
            string reg_nga = Request.Form["reg_nga"];
            string reg_wa = Request.Form["reg_wa"];
            string reg_member = Request.Form["reg_member"];
            string reg_income = Request.Form["reg_income"];
            string reg_capital = Request.Form["reg_capital"];
            string reg_accept = Request.Form["reg_accept"];
            string reg_organi_id = Request.Form["reg_organi_id"];

           


            //var reg_password = GenerateSHA512String(far_pass);


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            if (PROVINCE_ID != "0")
            {
                command.CommandText = "update regis_organi set " +
                    "type_organi_id='" + type_organi_id + "'," +
                    "reg_name='" + reg_name + "'," +
                    "reg_numberorgani='" + reg_numberorgani + "'," +
                    "reg_regis_name='" + reg_regis_name + "'," +
                    "reg_posi_name='" + reg_posi_name + "'," +
                    "DISTRICT_ID='" + DISTRICT_ID + "'," +
                    "reg_regis_tel='" + reg_regis_tel + "'," +
                    "reg_email='" + reg_email + "'," +
                    //"reg_chief='" + reg_chief + "'," +
                    //"reg_ctel='" + reg_ctel + "'," +
                    //"reg_vchief='" + reg_vchief + "'," +
                    //"reg_vctel='" + reg_vctel + "'," +
                    "reg_num='" + reg_num + "'," +
                    "reg_addr='" + reg_addr + "'," +
                    "PROVINCE_ID='" + PROVINCE_ID + "'," +
                    "AMPHUR_ID='" + AMPHUR_ID + "'," +
                    "DISTRICT_ID='" + DISTRICT_ID + "'" +
                    //"detail_opara='" + detail_opara + "'," +
                    //"activity_curent='" + activity_curent + "'," +
                    //"reg_results='" + reg_results + "'," +
                    //"reg_vision='" + reg_vision + "'," +
                    //"reg_plant='" + reg_plant + "'," +
                    //"reg_rai='" + reg_rai + "'," +
                    //"reg_nga='" + reg_nga + "'," +
                    //"reg_wa='" + reg_wa + "'," +
                    //"reg_member='" + reg_member + "'," +
                    //"reg_income='" + reg_income + "'," +
                    //"reg_capital='" + reg_capital + "'" +
                    "where reg_organi_id='" + reg_organi_id + "'";
            }
            else
            {
                command.CommandText = "update regis_organi set " +
                    "type_organi_id='" + type_organi_id + "'," +
                    "reg_name='" + reg_name + "'," +
                    "reg_numberorgani='" + reg_numberorgani + "'," +
                    "reg_regis_name='" + reg_regis_name + "'," +
                    "reg_posi_name='" + reg_posi_name + "'," +
                    "reg_regis_tel='" + reg_regis_tel + "'," +
                    "reg_email='" + reg_email + "'," +
                    //"reg_chief='" + reg_chief + "'," +
                    //"reg_ctel='" + reg_ctel + "'," +
                    //"reg_vchief='" + reg_vchief + "'," +
                    //"reg_vctel='" + reg_vctel + "'," +
                    "reg_num='" + reg_num + "'," +
                    "reg_addr='" + reg_addr + "'" +
                    //"detail_opara='" + detail_opara + "'," +
                    //"activity_curent='" + activity_curent + "'," +
                    //"reg_results='" + reg_results + "'," +
                    //"reg_vision='" + reg_vision + "'," +
                    //"reg_plant='" + reg_plant + "'," +
                    //"reg_rai='" + reg_rai + "'," +
                    //"reg_nga='" + reg_nga + "'," +
                    //"reg_wa='" + reg_wa + "'," +
                    //"reg_member='" + reg_member + "'," +
                    //"reg_income='" + reg_income + "'," +
                    //"reg_capital='" + reg_capital + "'" +
                    "where reg_organi_id='" + reg_organi_id + "'";

            }
            int i = command.ExecuteNonQuery();

          


            connection.Close();


            //return View("About");

            if (i >= 1)
            {


                Session["alert"] = "Yes";
                // return RedirectToAction("organi_update", "Home");

                if (Session["reg_organi_id"] != null)
                {
                    return RedirectToAction("organi_update", "Home");

                }
                else
                {
                    return RedirectToAction("organi_update/" + reg_organi_id, "Home");
                }

            }
            else
            {

                 Response.Write("<script language=javascript>alert('บันทึกไม่สำเร็จ');</script>");
                return View("index");
            
            }

        }


        public ActionResult register_owner_organi()
        {



            ViewBag.Message = "register_owner_organi";

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "select * from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }


            reader.Close();

            TempData["capchar"] = capchar(7);

            return View("Index", ownerland);



        }


        public ActionResult Submitregister_owner_organi()
        {

            string or_name = Request.Form["or_name"];
            string or_number = Request.Form["or_number"];
            string or_name_regis = Request.Form["or_name_regis"];
            string or_posi_regis = Request.Form["or_posi_regis"];
            string or_address = Request.Form["or_address"];
           

            string PROVINCE_ID = Request.Form["PROVINCE_ID"];
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];

            string or_phone_regis = Request.Form["or_phone_regis"];
            string or_email = Request.Form["or_email"];
            
            string or_pass = CreatePassword(5);

            var pass = GenerateSHA512String(or_pass);


           

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "insert into tb_owner_oragani " +
                "(or_name," +
                "or_number," +
                "or_name_regis," +
                "or_posi_regis," +
                "or_address," +
                "PROVINCE_ID," +
                "AMPHUR_ID," +
                "DISTRICT_ID," +
                "or_phone_regis," +
                "or_email," +
                "or_pass" +
               

                ")" +
                "VALUES " +
                "('" + or_name + "'," +
                "'" + or_number + "'," +
                "'" + or_name_regis + "'," +
                "'" + or_posi_regis + "'," +
                "'" + or_address + "'," +
                "'" + PROVINCE_ID + "'," +
                "'" + AMPHUR_ID + "'," +
                "'" + DISTRICT_ID + "'," +
                "'" + or_phone_regis + "'," +
                "'" + or_email + "'," +
                "'" + pass + "'" +
                ")";
            int i = command.ExecuteNonQuery();
            var id_owner_organi = command.LastInsertedId;

            var changeid = CreatePassword(3) + id_owner_organi + CreatePassword(50);
            connection.Close();


            if (i >= 1)
            {

                string Url = "wwww.google.com";


                TempData["data_owner"] = or_name + "," + or_pass + "," + or_email;
                TempData["Url"] = Url;

                //ViewBag.Message = "owner";
                //ViewBag.check = "true";

                TempData["owner_orga"] = "owner_orga";
                TempData["check"] = "true";
                TempData["id_owner_organi"] = changeid;



                return RedirectToAction("confrim_regis_orga", "Home");

            }
            else
            {

                TempData["owner_orga"] = "owner_orga";
                TempData["check"] = "false";

                return RedirectToAction("confrim_regis_orga", "Home");
            }

        }

        public ActionResult confrim_regis_orga()
        {

            ViewBag.Message = "confrim_regis_orga";

            return View("index");
        }


        public ActionResult confor_owgani(string id)
        {



            string reg_organi_id = id.Substring(3, id.Length - 53);
            ViewBag.textid = reg_organi_id;

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update tb_owner_oragani set " +
                     "or_confirm ='2'" +
                     " where id_owner_organi = '" + reg_organi_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                ViewBag.Message = "login";
                return View("index");
            }
            else
            {
                ViewBag.Message = "temindex";
                return View("index");
            }



        }


        public ActionResult update_db_owner_organi()
        {


            string id_owner_organi = Request.Form["id_owner_organi"];
            string or_name = Request.Form["or_name"];
            string or_number = Request.Form["or_number"];

            string or_name_regis = Request.Form["or_name_regis"];
            string or_posi_regis = Request.Form["or_posi_regis"];
            string or_address = Request.Form["or_address"];
            string PROVINCE_ID = Request.Form["PROVINCE_ID"]; 
            string AMPHUR_ID = Request.Form["amp_id"];
            string DISTRICT_ID = Request.Form["distr_id"];

            string or_phone_regis = Request.Form["or_phone_regis"];
           




            //var reg_password = GenerateSHA512String(far_pass);


            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            if (PROVINCE_ID != "0")
            {
                command.CommandText = "update tb_owner_oragani set " +
                    "or_name='" + or_name + "'," +
                    "or_number='" + or_number + "'," +
                    "or_name_regis='" + or_name_regis + "'," +
                    "or_posi_regis='" + or_posi_regis + "'," +
                    "or_address='" + or_address + "'," +
                    "PROVINCE_ID='" + PROVINCE_ID + "'," +
                    "AMPHUR_ID='" + AMPHUR_ID + "'," +
                    "DISTRICT_ID='" + DISTRICT_ID + "'," +
                    "or_phone_regis='" + or_phone_regis + "'" +
                    "where id_owner_organi='" + id_owner_organi + "'";
            }
            else
            {
                command.CommandText = "update tb_owner_oragani set " +
                    "or_name='" + or_name + "'," +
                    "or_number='" + or_number + "'," +
                    "or_name_regis='" + or_name_regis + "'," +
                    "or_posi_regis='" + or_posi_regis + "'," +
                    "or_address='" + or_address + "'," +
                    "or_phone_regis='" + or_phone_regis + "'" +
                    "where id_owner_organi='" + id_owner_organi + "'";

            }
            int i = command.ExecuteNonQuery();




            connection.Close();


            //return View("About");

            if (i >= 1)
            {


                Session["alert"] = "Yes";
                return RedirectToAction("data_owner_organi", "Home");

            }
            else
            {

                Response.Write("<script language=javascript>alert('บันทึกไม่สำเร็จ');</script>");
                return View("index");

            }

        }

        public ActionResult map_marker_all()
        {

             MySqlConnection connection = new MySqlConnection(constr);
             List<RegisterModel> land = new List<RegisterModel>();
             connection.Open();

             MySqlCommand command = connection.CreateCommand();

             command.CommandText = "select " +
                "t.name_type,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.type_land_id,ow.land_rai,ow.land_nga,ow.land_va,ow.period_per,ow.period,ow.codition,ow.latitude_land,ow.longitude_land,ow.place_land from owner_land as ow " +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
                "where ow.status_land='2'";

             MySqlDataReader reader = command.ExecuteReader();



             while (reader.Read())
             {

                 land.Add(new RegisterModel
                 {
                     o_land_id = Convert.ToInt32(reader["o_land_id"]),
                     PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                     AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                     DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                     dsave = reader["dsave"].ToString(),
                     m_rent = reader["m_rent"].ToString(),
                     place_land = reader["place_land"].ToString(),
                     type_land_id = Convert.ToInt32(reader["type_land_id"]),
                     land_rai = reader["land_rai"].ToString(),
                     land_nga = reader["land_nga"].ToString(),
                     land_va = reader["land_va"].ToString(),
                     period_per = reader["period_per"].ToString(),
                     period = reader["period"].ToString(),
                     codition = reader["codition"].ToString(),
                     latitude_land = reader["latitude_land"].ToString(),
                     longitude_land = reader["longitude_land"].ToString(),
                     name_type = reader["name_type"].ToString()

             });
             }


             reader.Close();
             
            ViewBag.land = land;
            return View("map_marker_all");

            //return Redirect("http://localhost/cmarket/apimap/map_api_mark.php");
            //return Redirect("http://intranet.labai.or.th:8080/apimail/apimap/map_api_mark.php");
           
        }


        public ActionResult wantland(int idkey)
        {

            ViewBag.Message = "wantland";
            ViewBag.wantkey = 1;




            MySqlConnection connection = new MySqlConnection(constr);
                List<RegisterModel> land = new List<RegisterModel>();
                
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                MySqlCommand command2 = connection.CreateCommand();
                MySqlCommand command3 = connection.CreateCommand();
                List<RegisterModel> ownerland = new List<RegisterModel>();
            if (idkey == 1)
            {
                command.CommandText = "SELECT PROVINCE_ID,PROVINCE_NAME,(SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID and type_farmer_regis='1') as countpro FROM province as pp order by PROVINCE_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID and type_farmer_regis='1')) as sumpro FROM province as pp";
                TempData["typeper"] = "บุคคลทั่วไป";
                TempData["typeperkey"] = idkey;
            }

            if (idkey == 2)
            {
                command.CommandText = "SELECT PROVINCE_ID,PROVINCE_NAME,(SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID and type_farmer_regis='2') as countpro FROM province as pp order by PROVINCE_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID and type_farmer_regis='2')) as sumpro FROM province as pp";
                TempData["typeper"] = "กลุ่มเกษตรกร";
                TempData["typeperkey"] = idkey;
            }

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
                {

                    land.Add(new RegisterModel
                    {
                        PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        id_land_use = Convert.ToInt32(reader["countpro"]),


                    });
                }
            reader.Close();

            MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
               

            }

            reader5.Close();

            command3.CommandText = "SELECT pp.PROVINCE_ID,pp.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME FROM province as pp inner join amphur as aa on(pp.PROVINCE_ID=aa.PROVINCE_ID) inner join district as dd on(dd.AMPHUR_ID=aa.AMPHUR_ID)";
            MySqlDataReader reader3 = command3.ExecuteReader();
            while (reader3.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader3["PROVINCE_ID"]),
                    PROVINCE_NAME = reader3["PROVINCE_NAME"].ToString(),
                    AMPHUR_ID = Convert.ToInt32(reader3["AMPHUR_ID"]),
                    AMPHUR_NAME = reader3["AMPHUR_NAME"].ToString(),
                    DISTRICT_ID = Convert.ToInt32(reader3["DISTRICT_ID"]),
                    DISTRICT_NAME = reader3["DISTRICT_NAME"].ToString(),



                });
            }
            reader3.Close();

            ViewBag.land = land;
            ViewBag.ownerland = ownerland;

            return View("index");

        }


        public ActionResult wantland_ampor(int id,int idkey)
        {

            ViewBag.Message = "wantland";
            ViewBag.wantkey = 2;



            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();
            MySqlCommand command3 = connection.CreateCommand();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();
            if (idkey == 1)
            {
                command.CommandText = "SELECT AMPHUR_ID,AMPHUR_NAME,(SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID and type_farmer_regis='1') as countpro FROM amphur as pp WHERE pp.PROVINCE_ID=" + id+ " order by pp.AMPHUR_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID and type_farmer_regis='1')) as sumpro,pr.PROVINCE_NAME FROM amphur as pp inner join province as pr on(pr.PROVINCE_ID=pp.PROVINCE_ID)  WHERE pp.PROVINCE_ID=" + id + "";
                TempData["typeper"] = "บุคคลทั่วไป";
                TempData["typeperkey"] = idkey;
            }

            if (idkey == 2)
            {
                command.CommandText = "SELECT AMPHUR_ID,AMPHUR_NAME,(SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID and type_farmer_regis='2') as countpro FROM amphur as pp WHERE pp.PROVINCE_ID=" + id + " order by pp.AMPHUR_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID and type_farmer_regis='2')) as sumpro,pr.PROVINCE_NAME FROM amphur as pp inner join province as pr on(pr.PROVINCE_ID=pp.PROVINCE_ID)  WHERE pp.PROVINCE_ID=" + id + "";
                TempData["typeper"] = "กลุ่มเกษตรกร";
                TempData["typeperkey"] = idkey;
            }

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    id_land_use = Convert.ToInt32(reader["countpro"]),
                    


                });
            }
            reader.Close();

            MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
                ViewData["PROVINCE_NAME"] = reader5["PROVINCE_NAME"].ToString();


            }

            reader5.Close();
            /*
            command3.CommandText = "SELECT pp.PROVINCE_ID,pp.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME FROM province as pp inner join amphur as aa on(pp.PROVINCE_ID=aa.PROVINCE_ID) inner join district as dd on(dd.AMPHUR_ID=aa.AMPHUR_ID)";
            MySqlDataReader reader3 = command3.ExecuteReader();
            while (reader3.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader3["PROVINCE_ID"]),
                    PROVINCE_NAME = reader3["PROVINCE_NAME"].ToString(),
                    AMPHUR_ID = Convert.ToInt32(reader3["AMPHUR_ID"]),
                    AMPHUR_NAME = reader3["AMPHUR_NAME"].ToString(),
                    DISTRICT_ID = Convert.ToInt32(reader3["DISTRICT_ID"]),
                    DISTRICT_NAME = reader3["DISTRICT_NAME"].ToString(),



                });
            }
            reader3.Close();
            ViewBag.ownerland = ownerland;
            */
            ViewBag.land = land;
            
            return View("index");

        }


        public ActionResult wantland_tambon(int id, int idkey)
        {

            ViewBag.Message = "wantland";
            ViewBag.wantkey = 3;



            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            connection.Open();
            MySqlCommand command3 = connection.CreateCommand();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();
            if (idkey == 1)
            {
                command.CommandText = "SELECT DISTRICT_ID,DISTRICT_NAME,(SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID and type_farmer_regis='1') as countpro FROM district as pp WHERE pp.AMPHUR_ID=" + id + " order by pp.DISTRICT_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID and type_farmer_regis='1')) as sumpro,pr.PROVINCE_NAME,aa.AMPHUR_NAME,pp.DISTRICT_NAME FROM district as pp inner join province as pr on(pr.PROVINCE_ID=pp.PROVINCE_ID) INNER JOIN amphur as aa on(aa.AMPHUR_ID=pp.AMPHUR_ID)  WHERE pp.AMPHUR_ID=" + id + "";
                TempData["typeper"] = "บุคคลทั่วไป";
                TempData["typeperkey"] = idkey;
            }

            if (idkey == 2)
            {
                command.CommandText = "SELECT DISTRICT_ID,DISTRICT_NAME,(SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID and type_farmer_regis='2') as countpro FROM district as pp WHERE pp.AMPHUR_ID=" + id + " order by pp.DISTRICT_NAME asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID and type_farmer_regis='2')) as sumpro,pr.PROVINCE_NAME,aa.AMPHUR_NAME,pp.DISTRICT_NAME FROM district as pp inner join province as pr on(pr.PROVINCE_ID=pp.PROVINCE_ID) INNER JOIN amphur as aa on(aa.AMPHUR_ID=pp.AMPHUR_ID)  WHERE pp.AMPHUR_ID=" + id + "";
                TempData["typeper"] = "กลุ่มเกษตรกร";
                TempData["typeperkey"] = idkey;
            }

            MySqlDataReader reader = command.ExecuteReader();


            while (reader.Read())
            {

                land.Add(new RegisterModel
                {
                    DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    id_land_use = Convert.ToInt32(reader["countpro"]),



                });
            }
            reader.Close();

            MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
                ViewData["PROVINCE_NAME"] = reader5["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader5["AMPHUR_NAME"].ToString();
                ViewData["DISTRICT_NAME"] = reader5["DISTRICT_NAME"].ToString();


            }

            reader5.Close();

            command3.CommandText = "SELECT pp.PROVINCE_ID,pp.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME FROM province as pp inner join amphur as aa on(pp.PROVINCE_ID=aa.PROVINCE_ID) inner join district as dd on(dd.AMPHUR_ID=aa.AMPHUR_ID)";
            MySqlDataReader reader3 = command3.ExecuteReader();
            while (reader3.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader3["PROVINCE_ID"]),
                    PROVINCE_NAME = reader3["PROVINCE_NAME"].ToString(),
                    AMPHUR_ID = Convert.ToInt32(reader3["AMPHUR_ID"]),
                    AMPHUR_NAME = reader3["AMPHUR_NAME"].ToString(),
                    DISTRICT_ID = Convert.ToInt32(reader3["DISTRICT_ID"]),
                    DISTRICT_NAME = reader3["DISTRICT_NAME"].ToString(),



                });
            }
            reader3.Close();

            ViewBag.land = land;
            ViewBag.ownerland = ownerland;
            return View("index");

        }


        public ActionResult show_want_per(int id, int idkey, string PROVINCE_NAME, string AMPHUR_NAME, string DISTRICT_NAME)
        {

            ViewBag.Message = "wantland";
            ViewBag.wantkey = 4;
            ViewBag.showarea = 0;
            ViewData["PROVINCE_NAME"] = PROVINCE_NAME;
            ViewData["AMPHUR_NAME"] = AMPHUR_NAME;
            ViewData["DISTRICT_NAME"] = DISTRICT_NAME;


            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            List<RegisterFarnerModel> landuser = new List<RegisterFarnerModel>();
            connection.Open();
            MySqlCommand command3 = connection.CreateCommand();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();

           
            if (idkey == 1)
            {
                
                command.CommandText = "SELECT re.far_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer FROM land_famer as lf left JOIN tb_regfarmer as re ON(lf.far_id=re.far_id) WHERE type_status_regis > 0 AND lf.DISTRICT_ID=" + id + " and lf.type_farmer_regis='1' order by re.far_id asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID AND type_farmer_regis = 1)) as sumpro FROM district as pp WHERE pp.DISTRICT_ID=" + id + "";
                TempData["typeper"] = "บุคคลทั่วไป";
                TempData["typeperkey"] = idkey;
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    land.Add(new RegisterModel
                    {
                        far_code = reader["far_code"].ToString(),
                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        f_wland = Convert.ToInt32(reader["f_wland"]),
                        id_land_famer = Convert.ToInt32(reader["id_land_famer"]),



                    });
                    
                }
                reader.Close();
            }

            
            if (idkey == 2)
            {
                
                command.CommandText = "SELECT re.oga_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer FROM land_famer as lf left JOIN regis_organi as re ON(lf.far_id=re.reg_organi_id) WHERE lf.DISTRICT_ID=" + id + " and re.type_status_regis_og='2' order by re.far_id asc";
                command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID AND type_farmer_regis = 2)) as sumpro FROM district as pp WHERE pp.DISTRICT_ID=" + id + "";
                TempData["typeper"] = "กลุ่มเกษตรกร";
                TempData["typeperkey"] = idkey;
                MySqlDataReader reader25 = command.ExecuteReader();
                while (reader25.Read())
                {

                    land.Add(new RegisterModel
                    {
                        far_code = reader25["oga_code"].ToString(),
                        land_rai = reader25["land_rai"].ToString(),
                        land_nga = reader25["land_nga"].ToString(),
                        land_va = reader25["land_va"].ToString(),
                        f_wland = Convert.ToInt32(reader25["f_wland"]),
                        id_land_famer = Convert.ToInt32(reader25["id_land_famer"]),



                    });
                    
                }
                reader25.Close();
            }

            


            
           

            /*MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
                ViewData["PROVINCE_NAME"] = reader5["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader5["AMPHUR_NAME"].ToString();


            }

            reader5.Close();*/


            command.CommandText = "select * from r_farmer_land_use as us inner join tb_land_use as tu on(tu.id_land_use=us.id_land_use)";
            MySqlDataReader reader2 = command.ExecuteReader();
            while (reader2.Read())
            {

                landuser.Add(new RegisterFarnerModel
                {
                    id_land_famer = Convert.ToInt32(reader2["id_land_famer"]),
                    id_land_use = Convert.ToInt32(reader2["id_land_use"]),
                    land_use_name = reader2["land_use_name"].ToString(),
                    fdetail_user_land = reader2["fdetail_user_land"].ToString(),



                });
            }

            reader2.Close();

           /* command3.CommandText = "SELECT pp.PROVINCE_ID,pp.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME FROM province as pp inner join amphur as aa on(pp.PROVINCE_ID=aa.PROVINCE_ID) inner join district as dd on(dd.AMPHUR_ID=aa.AMPHUR_ID)";
            MySqlDataReader reader3 = command3.ExecuteReader();
            while (reader3.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader3["PROVINCE_ID"]),
                    PROVINCE_NAME = reader3["PROVINCE_NAME"].ToString(),
                    AMPHUR_ID = Convert.ToInt32(reader3["AMPHUR_ID"]),
                    AMPHUR_NAME = reader3["AMPHUR_NAME"].ToString(),
                    DISTRICT_ID = Convert.ToInt32(reader3["DISTRICT_ID"]),
                    DISTRICT_NAME = reader3["DISTRICT_NAME"].ToString(),



                });
            }
            reader3.Close();
            */
            
            ViewBag.ownerland = ownerland;

            ViewBag.landuser = landuser;
            ViewBag.land = land;
            return View("index");

        }


        public ActionResult show_want_per2()
        {

            ViewBag.Message = "wantland";
            ViewBag.wantkey = 4;
            ViewBag.showarea = 1;
            string[] DateArray = Url.RequestContext.RouteData.Values["id"].ToString().Split('-');
            string id = DateArray[0];
            string idkey = DateArray[1];
            string keysearch = DateArray[2];



            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> land = new List<RegisterModel>();
            List<RegisterFarnerModel> landuser = new List<RegisterFarnerModel>();
            connection.Open();
            MySqlCommand command3 = connection.CreateCommand();
            List<RegisterModel> ownerland = new List<RegisterModel>();
            MySqlCommand command = connection.CreateCommand();
            MySqlCommand command2 = connection.CreateCommand();


            if (Convert.ToInt32(idkey) == 1)
            {
                if (Convert.ToInt32(keysearch) == 1)
                {
                    command.CommandText = "SELECT re.far_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN tb_regfarmer as re ON(lf.far_id=re.far_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE type_status_regis > 0 AND lf.PROVINCE_ID=" + Convert.ToInt32(id) + " and lf.type_farmer_regis='1' order by re.far_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID AND type_farmer_regis = 1)) as sumpro FROM province as pp WHERE pp.PROVINCE_ID=" + Convert.ToInt32(id) + "";

                }
                if (Convert.ToInt32(keysearch) == 2)
                {
                    command.CommandText = "SELECT re.far_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN tb_regfarmer as re ON(lf.far_id=re.far_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE type_status_regis > 0 AND lf.AMPHUR_ID=" + Convert.ToInt32(id) + " and lf.type_farmer_regis='1' order by re.far_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID AND type_farmer_regis = 1)) as sumpro FROM amphur as pp WHERE pp.AMPHUR_ID=" + Convert.ToInt32(id) + "";

                }
                if (Convert.ToInt32(keysearch) == 3)
                {
                    command.CommandText = "SELECT re.far_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN tb_regfarmer as re ON(lf.far_id=re.far_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE type_status_regis > 0 AND lf.DISTRICT_ID=" + Convert.ToInt32(id) + " and lf.type_farmer_regis='1' order by re.far_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID AND type_farmer_regis = 1)) as sumpro FROM district as pp WHERE pp.DISTRICT_ID=" + Convert.ToInt32(id) + "";

                }


                TempData["typeper"] = "บุคคลทั่วไป";
                TempData["typeperkey"] = idkey;
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    land.Add(new RegisterModel
                    {
                        far_code = reader["far_code"].ToString(),
                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        f_wland = Convert.ToInt32(reader["f_wland"]),
                        id_land_famer = Convert.ToInt32(reader["id_land_famer"]),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),



                    });

                }
                reader.Close();
            }




            if (Convert.ToInt32(idkey) == 2)
            {

                if (Convert.ToInt32(keysearch) == 1)
                {

                    command.CommandText = "SELECT re.oga_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN regis_organi as re ON(lf.far_id=re.reg_organi_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE lf.PROVINCE_ID=" + id + " and re.type_status_regis_og='2' order by re.reg_organi_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE PROVINCE_ID = pp.PROVINCE_ID AND type_farmer_regis = 2)) as sumpro FROM province as pp WHERE pp.PROVINCE_ID=" + id + "";

                }
                if (Convert.ToInt32(keysearch) == 2)
                {
                    command.CommandText = "SELECT re.oga_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN regis_organi as re ON(lf.far_id=re.reg_organi_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE lf.AMPHUR_ID=" + id + " and re.type_status_regis_og='2' order by re.reg_organi_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE AMPHUR_ID = pp.AMPHUR_ID AND type_farmer_regis = 2)) as sumpro FROM amphur as pp WHERE pp.AMPHUR_ID=" + id + "";

                }
                if (Convert.ToInt32(keysearch) == 3)
                {
                    command.CommandText = "SELECT re.oga_code,lf.land_rai,lf.land_nga,lf.land_va,lf.f_wland,lf.id_land_famer,pp.PROVINCE_NAME,aa.AMPHUR_NAME,dd.DISTRICT_NAME FROM land_famer as lf left JOIN regis_organi as re ON(lf.far_id=re.reg_organi_id) INNER JOIN province as pp on(pp.PROVINCE_ID=lf.PROVINCE_ID) INNER JOIN amphur as aa ON(aa.AMPHUR_ID=lf.AMPHUR_ID) INNER JOIN district as dd ON(dd.DISTRICT_ID=lf.DISTRICT_ID) WHERE lf.DISTRICT_ID=" + id + " and re.type_status_regis_og='2' order by re.reg_organi_id asc";
                    command2.CommandText = "SELECT sum((SELECT COUNT(*) FROM land_famer WHERE DISTRICT_ID = pp.DISTRICT_ID AND type_farmer_regis = 2)) as sumpro FROM district as pp WHERE pp.DISTRICT_ID=" + id + "";

                }

                TempData["typeper"] = "กลุ่มเกษตรกร";
                TempData["typeperkey"] = idkey;
                MySqlDataReader reader25 = command.ExecuteReader();
                while (reader25.Read())
                {

                    land.Add(new RegisterModel
                    {
                        far_code = reader25["oga_code"].ToString(),
                        land_rai = reader25["land_rai"].ToString(),
                        land_nga = reader25["land_nga"].ToString(),
                        land_va = reader25["land_va"].ToString(),
                        f_wland = Convert.ToInt32(reader25["f_wland"]),
                        id_land_famer = Convert.ToInt32(reader25["id_land_famer"]),
                        PROVINCE_NAME = reader25["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader25["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader25["DISTRICT_NAME"].ToString(),



                    });

                }
                reader25.Close();
            }



            MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
               


            }

            reader5.Close();



            /*MySqlDataReader reader5 = command2.ExecuteReader();
            if (reader5.Read())
            {
                ViewData["sumpro"] = Convert.ToInt32(reader5["sumpro"]);
                ViewData["PROVINCE_NAME"] = reader5["PROVINCE_NAME"].ToString();
                ViewData["AMPHUR_NAME"] = reader5["AMPHUR_NAME"].ToString();


            }

            reader5.Close();*/


            command.CommandText = "select * from r_farmer_land_use as us inner join tb_land_use as tu on(tu.id_land_use=us.id_land_use)";
            MySqlDataReader reader2 = command.ExecuteReader();
            while (reader2.Read())
            {

                landuser.Add(new RegisterFarnerModel
                {
                    id_land_famer = Convert.ToInt32(reader2["id_land_famer"]),
                    id_land_use = Convert.ToInt32(reader2["id_land_use"]),
                    land_use_name = reader2["land_use_name"].ToString(),
                    fdetail_user_land = reader2["fdetail_user_land"].ToString(),



                });
            }

            reader2.Close();

            /* command3.CommandText = "SELECT pp.PROVINCE_ID,pp.PROVINCE_NAME,aa.AMPHUR_ID,aa.AMPHUR_NAME,dd.DISTRICT_ID,dd.DISTRICT_NAME FROM province as pp inner join amphur as aa on(pp.PROVINCE_ID=aa.PROVINCE_ID) inner join district as dd on(dd.AMPHUR_ID=aa.AMPHUR_ID)";
             MySqlDataReader reader3 = command3.ExecuteReader();
             while (reader3.Read())
             {

                 ownerland.Add(new RegisterModel
                 {
                     PROVINCE_ID = Convert.ToInt32(reader3["PROVINCE_ID"]),
                     PROVINCE_NAME = reader3["PROVINCE_NAME"].ToString(),
                     AMPHUR_ID = Convert.ToInt32(reader3["AMPHUR_ID"]),
                     AMPHUR_NAME = reader3["AMPHUR_NAME"].ToString(),
                     DISTRICT_ID = Convert.ToInt32(reader3["DISTRICT_ID"]),
                     DISTRICT_NAME = reader3["DISTRICT_NAME"].ToString(),



                 });
             }
             reader3.Close();
             */

            ViewBag.ownerland = ownerland;

            ViewBag.landuser = landuser;
            ViewBag.land = land;
            return View("index");

        }


        public ActionResult loadprov_addr()
        {
            
           

            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select PROVINCE_ID,PROVINCE_NAME from province";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    PROVINCE_ID = Convert.ToInt32(reader["PROVINCE_ID"]),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }



            reader.Close();
            ViewBag.Message = "provsearch";
            return View("getaddr_search", ownerland);


        }


        public ActionResult loadamp_addr()
        {



            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select AMPHUR_ID,AMPHUR_NAME,PROVINCE_NAME from amphur as aa inner join province as pp on(pp.PROVINCE_ID=aa.PROVINCE_ID)";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    AMPHUR_ID = Convert.ToInt32(reader["AMPHUR_ID"]),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),

                });
            }



            reader.Close();
            ViewBag.Message = "ampsearch";
            return View("getaddr_search", ownerland);


        }

        public ActionResult loaddist_addr()
        {



            MySqlConnection connection = new MySqlConnection(constr);
            List<RegisterModel> ownerland = new List<RegisterModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "select DISTRICT_ID,DISTRICT_NAME,AMPHUR_NAME,PROVINCE_NAME from district as dd inner join amphur as aa on(dd.AMPHUR_ID=aa.AMPHUR_ID) inner join province as pp on(pp.PROVINCE_ID=aa.PROVINCE_ID)";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                ownerland.Add(new RegisterModel
                {
                    DISTRICT_ID = Convert.ToInt32(reader["DISTRICT_ID"]),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),

                });
            }



            reader.Close();
            ViewBag.Message = "distsearch";
            return View("getaddr_search", ownerland);


        }
        


    }
}