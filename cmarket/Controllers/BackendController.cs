﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cmarket.Models;
using MySql.Data.MySqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using PagedList;



namespace cmarket.Controllers
{
    public class BackendController : Controller
    {
        // GET: Backend
        string constr = ConfigurationManager.ConnectionStrings["connectmysql"].ConnectionString;
        public ActionResult Index()
        {
            //ViewData["data"] = "pindex";
            //TempData["data"]
            TempData["data"] = "pindex";
            return View("index");
        }

        // GET: Backend/Details/5
        public ActionResult owner_land(int type_status_regis)
        {
            //ViewBag.Message = "owner_land";
            TempData["data"] = "owner_land";
            TempData["keymenu"] = "owner_land";
            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> owland = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            if (type_status_regis==1) { 
            command.CommandText = "select * from tb_regfarmer as ow " +
                 "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                 "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                 "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID) where type_status_regis='1' order by ow.far_id asc";
            }

            if (type_status_regis == 2)
            {
                command.CommandText = "select * from tb_regfarmer as ow " +
                     "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                     "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                     "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID) where type_status_regis='2' order by ow.far_id asc";
            }

            MySqlDataReader reader = command.ExecuteReader();



            while (reader.Read())
            {

                owland.Add(new adminModel
                {
                    far_id = Convert.ToInt32(reader["far_id"]),
                    far_name = reader["far_name"].ToString(),
                    far_surname = reader["far_surname"].ToString(),
                    id_card = reader["id_card"].ToString(),
                    far_tel = reader["far_tel"].ToString(),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                 
                    

                });
            }


            reader.Close();
            ViewBag.owland = owland;
            ViewData["type_status_regis"] = type_status_regis;
            return View("index");
        }



        public ActionResult owner_land_orga(int type_status_regis_og)
        {
            //ViewBag.Message = "owner_land";
            TempData["data"] = "owner_land_orga";
            TempData["keymenu"] = "owner_land_orga";
            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> owland = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            if (type_status_regis_og == 1)
            {
                command.CommandText = "select * from regis_organi as ow " +
                 "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                 "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                 "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID) where type_status_regis_og='1' order by ow.reg_organi_id asc";
            }

            if (type_status_regis_og == 2)
            {
                command.CommandText = "select * from regis_organi as ow " +
                 "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                 "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                 "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID) where type_status_regis_og='2' order by ow.reg_organi_id asc";
            }
            MySqlDataReader reader = command.ExecuteReader();



            while (reader.Read())
            {

                owland.Add(new adminModel
                {
                    reg_organi_id = Convert.ToInt32(reader["reg_organi_id"]),
                    reg_name = reader["reg_name"].ToString(),
                    reg_regis_name = reader["reg_regis_name"].ToString(),
                    reg_regis_tel = reader["reg_regis_tel"].ToString(),
                    PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                    AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                    DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                    reg_status = reader["reg_status"].ToString(),



                });
            }


            reader.Close();
            ViewBag.owland = owland;
            ViewData["type_status_regis_og"] = type_status_regis_og;
            return View("index");
        }

        public ActionResult landOwner(int type_owner_land)
        {
            TempData["data"] = "landOwner";
            TempData["keymenu"] = "landOwner";
            TempData["type_owner_land"] = type_owner_land;
            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> land = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (type_owner_land == 1) { 
                command.CommandText = "select " +
                "t.name_type,po.far_name,po.far_surname,ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave,ow.m_rent,ow.place_land,ow.status_land,ow.land_rai,ow.land_nga,ow.land_va,ow.o_land_num from owner_land as ow " +
                "left join tb_regfarmer as po on(po.far_id=ow.id_owner)" +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
                "where ow.type_owner_land='" + type_owner_land + "'" +

                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave order by ow.o_land_id,po.far_id,ow.status_land asc";

                MySqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    land.Add(new adminModel
                    {
                        o_land_id = Convert.ToInt32(reader["o_land_id"]),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                        dsave = reader["dsave"].ToString(),

                        m_rent = reader["m_rent"].ToString(),
                        place_land = reader["place_land"].ToString(),
                        status_land = reader["status_land"].ToString(),
                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        o_name = reader["far_name"].ToString(),
                        o_surname = reader["far_surname"].ToString(),
                        name_type = reader["name_type"].ToString(),
                        o_land_num = reader["o_land_num"].ToString(),


                    });
                }
                reader.Close();

            }

            if (type_owner_land == 2)
            {
                command.CommandText = "select * from owner_land as ow " +
                "left join regis_organi as po on(po.reg_organi_id=ow.id_owner)" +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "left join type_land as t on(t.type_land_id=ow.type_land_id)" +
                "where ow.type_owner_land='" + type_owner_land + "'" +

                "GROUP BY ow.o_land_id,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME,ow.dsave order by po.reg_organi_id,ow.status_land asc";

                MySqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    land.Add(new adminModel
                    {
                        o_land_id = Convert.ToInt32(reader["o_land_id"]),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),
                        dsave = reader["dsave"].ToString(),

                        m_rent = reader["m_rent"].ToString(),
                        place_land = reader["place_land"].ToString(),
                        status_land = reader["status_land"].ToString(),
                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        o_name = reader["reg_name"].ToString(),
                        //o_surname = reader["o_surname"].ToString(),
                        name_type = reader["name_type"].ToString(),


                    });
                }
                reader.Close();

            }


            

            ViewBag.land = land;
            ViewBag.type_owner_land = type_owner_land;
            return View("index");
        }

        // GET: Backend/Create
        public ActionResult ladmin()
        {
            
            return View("login");
        }


        public ActionResult focus_land(int o_land_id, string PROVINCE_NAME, string AMPHUR_NAME, string DISTRICT_NAME, string type_owner_land)
        {
            //ViewBag.Message = "owner_land";
            TempData["data"] = "focus_land";
            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> owland = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "select * from tb_polygon_owner where o_land_id='"+ o_land_id + "' ORDER BY seq_number asc";

            MySqlDataReader reader = command.ExecuteReader();



            while (reader.Read())
            {

                owland.Add(new adminModel
                {
                    polygon_land_id = Convert.ToInt32(reader["polygon_land_id"]),
                    polygon_lata = reader["polygon_lata"].ToString(),
                    polygon_ing = reader["polygon_ing"].ToString(),
                    o_land_id = Convert.ToInt32(reader["o_land_id"]),
                    seq_number = Convert.ToInt32(reader["seq_number"]),




                });
            }


            reader.Close();


            command.CommandText = "select * from owner_land where o_land_id='" + o_land_id + "' ";
            MySqlDataReader reader5 = command.ExecuteReader();


            if (reader5.Read())
            {
                ViewData["latitude_land"] = reader5["latitude_land"];
                ViewData["longitude_land"] = reader5["longitude_land"];
            }
            reader5.Close();


            ViewBag.owland = owland;

            ViewBag.o_land_id = o_land_id;
            ViewBag.PROVINCE_NAME = PROVINCE_NAME;
            ViewBag.AMPHUR_NAME = AMPHUR_NAME;
            ViewBag.DISTRICT_NAME = DISTRICT_NAME;
            ViewBag.type_owner_land = type_owner_land;

            return View("index");
        }

        public ActionResult updatelalo()
        {

         
            string o_land_idx = Request.Form["o_land_id"];
            string PROVINCE_NAME = Request.Form["PROVINCE_NAME"];
            string AMPHUR_NAME = Request.Form["AMPHUR_NAME"];
            string DISTRICT_NAME = Request.Form["DISTRICT_NAME"];
            string type_owner_land = Request.Form["type_owner_land"];

            string latitude = Request.Form["latitude"];
            string longitude = Request.Form["longitude"];

            MySqlConnection connection = new MySqlConnection(constr);

            connection.Open();

            MySqlCommand command = connection.CreateCommand();

           
                command.CommandText = "update owner_land set " +
                    "latitude_land='" + latitude + "'," +
                    "longitude_land='" + longitude + "'" +

                    "where o_land_id='" + o_land_idx + "' and type_owner_land='"+ type_owner_land + "'";

            
            int i = command.ExecuteNonQuery();



            connection.Close();


            Session["alert"] = "Yes";

            return RedirectToAction("focus_land", "Backend", new { o_land_id = o_land_idx, PROVINCE_NAME, AMPHUR_NAME, DISTRICT_NAME, type_owner_land });




        }

        public ActionResult updatepolygon()
        {


            string o_land_idx = Request.Form["o_land_id"];
            string PROVINCE_NAME = Request.Form["PROVINCE_NAME"];
            string AMPHUR_NAME = Request.Form["AMPHUR_NAME"];
            string DISTRICT_NAME = Request.Form["DISTRICT_NAME"];
            string type_owner_land = Request.Form["type_owner_land"];

            string latitude = Request.Form["latitude"];
            string longitude = Request.Form["longitude"];

            string seq_number = Request.Form["seq_number"];

            MySqlConnection connection = new MySqlConnection(constr);

            connection.Open();

            MySqlCommand command = connection.CreateCommand();


            command.CommandText = "insert into tb_polygon_owner " +
                    "(polygon_lata," +
                    "polygon_ing," +
                     "o_land_id," +
                     "seq_number" +

                    ")" +
                    "VALUES " +
                    "(" +

                    "'" + latitude + "'," +
                    "'" + longitude + "'," +
                    "'" + o_land_idx + "'," +
                    "'" + seq_number + "'" +


                    ")";


            int i = command.ExecuteNonQuery();



            connection.Close();

            Session["alert"] = "Yes";
           

            return RedirectToAction("focus_land", "Backend", new { o_land_id = o_land_idx, PROVINCE_NAME, AMPHUR_NAME, DISTRICT_NAME, type_owner_land });




        }

        public ActionResult delete_polygon(int polygon_land_id, int o_land_id1, string PROVINCE_NAME1, string AMPHUR_NAME1, string DISTRICT_NAME1, string type_owner_land1)
        {

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "delete from tb_polygon_owner where polygon_land_id='" + polygon_land_id + "'";


                int i = command.ExecuteNonQuery();
          

            connection.Close();

            Session["alert"] = "Del";

            return RedirectToAction("focus_land", "Backend", new { o_land_id = o_land_id1, PROVINCE_NAME = PROVINCE_NAME1, AMPHUR_NAME = AMPHUR_NAME1, DISTRICT_NAME = DISTRICT_NAME1, type_owner_land = type_owner_land1 });
        }


        public ActionResult checklogin()
        {
            string s_email = Request.Form["email"];
            string pass = Request.Form["pass"];

            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> login = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            //var ensc = GenerateSHA512String(o_pass);

            command.CommandText = "select * from tb_staff where s_email='" + s_email + "' and pass ='" + pass+"'" ;
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {

                TempData["data"] = "landOwner";
                Session["staff_id_enc"] = CreatePassword(3) + reader["staff_id"] + CreatePassword(50);
                Session["staff_id"] = Convert.ToInt32(reader["staff_id"]);
                Session["s_name"] = reader["s_name"].ToString();
                Session["s_surname"] = reader["s_surname"].ToString();
                Session["s_email"] = reader["s_email"].ToString();

                reader.Close();
                return RedirectToAction("owner_land", "Backend", new { type_status_regis = 1 });
                //return View("Index");
            }
            else
            {
                //ViewData["nodata"] = "ไม่พบข้อมูล";
                ViewBag.Message = "login";
                ViewBag.Alert = "NoData";
                Response.Write("<script language=javascript>alert('ไม่พบข้อมูล')</script>");
                //Response.Write("<script language=javascript>window.location.href = '@Url.Action('login', 'Home')'</script>");
                //Response.RedirectPermanent("@Url.Action('login', 'Home')");
                //return RedirectToAction("login", "Home");

                return RedirectToAction("ladmin", "Backend");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scrips", "<script>alert('ไม่พบข้อมูล');</script>");
            }

            //return Redirect("http://intranet.labai.or.th:8080/cmarket/checkad/chack_ad.php?username="+ email+ "&pass="+ pass);
        }


        public ActionResult resetpass_farmer(string id)
        {



            string[] DateArray = id.Split('-');
            string far_id = DateArray[0];
            string far_pass = DateArray[1];

            var pass = GenerateSHA512String(far_pass);

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update tb_regfarmer set " +
                     "far_pass ='" + pass + "'" +
                     " where far_id = '" + far_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {
                
                return Content("1");
            }
            else
            {
                
                return Content("0");
            }



        }


        public ActionResult resetpass_farmer_oga(string id)
        {



            string[] DateArray = id.Split('-');
            string reg_organi_id = DateArray[0];
            string reg_password = DateArray[1];

            var pass = GenerateSHA512String(reg_password);

            MySqlConnection connection = new MySqlConnection(constr);
            connection.Open();
            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "update regis_organi set " +
                     "reg_password ='" + pass + "'" +
                     " where reg_organi_id = '" + reg_organi_id + "'";


            int i = command.ExecuteNonQuery();
            connection.Close();

            if (i >= 1)
            {

                return Content("1");
            }
            else
            {

                return Content("0");
            }



        }


        public ActionResult farwantland(int type_farmer_regis)
        {
           
            TempData["keymenu"] = "wantland";
            TempData["type_farmer_regis"] = type_farmer_regis;
            MySqlConnection connection = new MySqlConnection(constr);
            List<adminModel> land = new List<adminModel>();
            List<adminModel> landuser = new List<adminModel>();
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            if (type_farmer_regis == 1)
            {
                TempData["data"] = "farwantland";
                command.CommandText = "select * from land_famer as ow " +
                "left join tb_regfarmer as po on(po.far_id=ow.far_id)" +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where ow.type_farmer_regis='" + type_farmer_regis + "'" +

                "GROUP BY ow.id_land_famer,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME order by ow.id_land_famer,po.far_id asc";

                MySqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    land.Add(new adminModel
                    {
                        id_land_famer = Convert.ToInt32(reader["id_land_famer"]),
                        far_name = reader["far_name"].ToString(),
                        far_surname = reader["far_surname"].ToString(),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),


                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        type_land_id = Convert.ToInt32(reader["type_land_id"]),
                        type_farmer_regis = reader["type_farmer_regis"].ToString(),
                        f_wland = reader["f_wland"].ToString(),
                        reg_member = reader["reg_member"].ToString(),
                        reg_income = reader["reg_income"].ToString(),
                        reg_capital = reader["reg_capital"].ToString(),
                        far_tel = reader["far_tel"].ToString(),

                    });
                }
                reader.Close();

            }

            if (type_farmer_regis == 2)
            {
                TempData["data"] = "farwantlandog";
                command.CommandText = "select * from land_famer as ow " +
                "left join regis_organi as po on(po.reg_organi_id=ow.far_id)" +
                "left join province as p on(p.PROVINCE_ID=ow.PROVINCE_ID)" +
                "left join amphur as a on(a.AMPHUR_ID=ow.AMPHUR_ID)" +
                "left join district as d on(d.DISTRICT_ID=ow.DISTRICT_ID)" +
                "where ow.type_farmer_regis='" + type_farmer_regis + "'" +

                "GROUP BY ow.id_land_famer,p.PROVINCE_NAME,a.AMPHUR_NAME,d.DISTRICT_NAME order by ow.id_land_famer,po.reg_organi_id asc";

                MySqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    land.Add(new adminModel
                    {
                        id_land_famer = Convert.ToInt32(reader["id_land_famer"]),
                        reg_name = reader["reg_name"].ToString(),
                        PROVINCE_NAME = reader["PROVINCE_NAME"].ToString(),
                        AMPHUR_NAME = reader["AMPHUR_NAME"].ToString(),
                        DISTRICT_NAME = reader["DISTRICT_NAME"].ToString(),


                        land_rai = reader["land_rai"].ToString(),
                        land_nga = reader["land_nga"].ToString(),
                        land_va = reader["land_va"].ToString(),
                        type_land_id = Convert.ToInt32(reader["type_land_id"]),
                        type_farmer_regis = reader["type_farmer_regis"].ToString(),
                        f_wland = reader["f_wland"].ToString(),
                        reg_member = reader["reg_member"].ToString(),
                        reg_income = reader["reg_income"].ToString(),
                        reg_capital = reader["reg_capital"].ToString(),
                        reg_regis_tel = reader["reg_regis_tel"].ToString(),


                    });
                }
                reader.Close();

            }


            command.CommandText = "select * from r_farmer_land_use as us inner join tb_land_use as tu on(tu.id_land_use=us.id_land_use)";
            MySqlDataReader reader2 = command.ExecuteReader();
            while (reader2.Read())
            {

                landuser.Add(new adminModel
                {
                    id_land_famer = Convert.ToInt32(reader2["id_land_famer"]),
                    id_land_use = Convert.ToInt32(reader2["id_land_use"]),
                    land_use_name = reader2["land_use_name"].ToString(),



                });
            }



            reader2.Close();

            ViewBag.landuser = landuser;
            ViewBag.land = land;
            ViewBag.type_farmer_regis = type_farmer_regis;
            return View("index");
        }

        public static string GenerateSHA512String(string input)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(input);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }


        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        // POST: Backend/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Backend/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Backend/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Backend/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Backend/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
